import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazPedidosProductos {

	private JFrame frame;
	private JTextField textField;
	InterfacesListaProductos ir;
	InterfazPedidosProductos ir2;
	InterfazPrincipal irPrincipal;
	
	int TotalCesta = 0,
		Pintado = 0;
	String ProductoBuscar,
		   Producto1,
		   Producto2,
		   Producto3,
		   usuario,
		   contrasenia;

	boolean Producto1Cesta,
			Producto2Cesta,
			Producto3Cesta;
	
	int Cesta = 0;
	String ProductoImg = "Imagen del producto",
		   ProductoNombre = "Nombre del producto";
	double ProductoPrecio = 15.6,
		   PrecioTotal = 0;
	
	DecimalFormat DosDecimales = new DecimalFormat("#.00");
	int Posicion = 71,
		ProductoStock = 0;

	/**
	 * Create the application.
	 */
	public InterfazPedidosProductos(InterfacesListaProductos ir, String Producto1, String Producto2, String Producto3, boolean Producto1Cesta, boolean Producto2Cesta, boolean Producto3Cesta, 
									int TotalCesta, String usuario, String contrasenia) {
		setProducto1(Producto1);
		setProducto2(Producto2);
		setProducto3(Producto3);
		setUsuario(usuario);
		setContrasenia(contrasenia);
		setProducto1Cesta(Producto1Cesta);
		setProducto2Cesta(Producto2Cesta);
		setProducto3Cesta(Producto3Cesta);
		setTotalCesta(TotalCesta);
		Cesta(Producto1);
	    initialize();
	    
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
        frame.setTitle("Menu Pedido");
		frame.setBounds(100, 100, 966, 839);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		
		JLabel label = new JLabel("Busqueda");
		label.setFont(new Font("Sylfaen", Font.BOLD, 16));
		label.setBounds(10, 11, 70, 22);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\uD83D\uDD0D");
		label_1.setBounds(374, 8, 50, 29);
		frame.getContentPane().add(label_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(90, 12, 304, 20);
		frame.getContentPane().add(textField);
		
				
		if (TotalCesta == 0) {
			JLabel lblNewLabel_TituloCestaVacia = new JLabel("Tu cesta est\u00E1 vac\u00EDa.");
			lblNewLabel_TituloCestaVacia.setBorder(null);
			lblNewLabel_TituloCestaVacia.setFont(new Font("Times New Roman", Font.BOLD, 20));
			lblNewLabel_TituloCestaVacia.setBounds(20, 71, 210, 29);
			frame.getContentPane().add(lblNewLabel_TituloCestaVacia);
			
			JLabel lblNewLabel_TextoCestaVacia = new JLabel("Haz que tu cesta de compra sea \u00FAtil: ll\u00E9nala de libros, CD, v\u00EDdeos, DVD, juguetes, productos electr\u00F3nicos y otros productos. \r\nSiga comprando en BuyMe.es.");
			lblNewLabel_TextoCestaVacia.setFont(new Font("Tahoma", Font.PLAIN, 11));
			lblNewLabel_TextoCestaVacia.setBounds(20, 196, 692, 13);
			frame.getContentPane().add(lblNewLabel_TextoCestaVacia);
		}
		
		else {			
			if(Producto1Cesta) {
			Image imgProducto1 = new ImageIcon(ProductoImg).getImage();
			ImageIcon img2Producto1 = new ImageIcon(imgProducto1.getScaledInstance(200, 200, Image.SCALE_SMOOTH));

			JLabel lbl_ProdImagen1 = new JLabel("");
			lbl_ProdImagen1.setIcon(img2Producto1);
			lbl_ProdImagen1.setToolTipText("imagen");
			lbl_ProdImagen1.setBorder(null);
			lbl_ProdImagen1.setForeground(new Color(0, 0, 0));
			lbl_ProdImagen1.setBackground(new Color(0, 0, 0));
			lbl_ProdImagen1.setBounds(25, 62, 210, 154);
			frame.getContentPane().add(lbl_ProdImagen1);

			JLabel lbl_ProdNombre1 = new JLabel(Producto1);
			lbl_ProdNombre1.setFont(new Font("Tahoma", Font.BOLD, 13));
			lbl_ProdNombre1.setToolTipText("nombre descripcion");
			lbl_ProdNombre1.setForeground(Color.BLACK);
			lbl_ProdNombre1.setBorder(null);
			lbl_ProdNombre1.setBackground(Color.BLACK);
			lbl_ProdNombre1.setBounds(235, 26, 210, 154);
			frame.getContentPane().add(lbl_ProdNombre1);

			JLabel lblPrecio1 = new JLabel(DosDecimales.format(ProductoPrecio) + " �");
			lblPrecio1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
			lblPrecio1.setToolTipText("precio");
			lblPrecio1.setForeground(Color.BLACK);
			lblPrecio1.setBorder(null);
			lblPrecio1.setBackground(Color.BLACK);
			lblPrecio1.setBounds(235, 71, 210, 154);
			frame.getContentPane().add(lblPrecio1);

			PrecioTotal += ProductoPrecio;
			}
			
			if(Producto2Cesta) {
				Cesta(Producto2);
				
			Image imgProducto2 = new ImageIcon(ProductoImg).getImage();
			ImageIcon img2Producto2 = new ImageIcon(imgProducto2.getScaledInstance(200, 200, Image.SCALE_SMOOTH));

			JLabel lbl_ProdImagen2 = new JLabel("");
			lbl_ProdImagen2.setIcon(img2Producto2);
			lbl_ProdImagen2.setToolTipText("imagen");
			lbl_ProdImagen2.setBorder(null);
			lbl_ProdImagen2.setForeground(new Color(0, 0, 0));
			lbl_ProdImagen2.setBackground(new Color(0, 0, 0));
			lbl_ProdImagen2.setBounds(25, 241, 210, 154);
			frame.getContentPane().add(lbl_ProdImagen2);

			JLabel lbl_ProdNombre2 = new JLabel(Producto2);
			lbl_ProdNombre2.setFont(new Font("Tahoma", Font.BOLD, 13));
			lbl_ProdNombre2.setToolTipText("nombre descripcion");
			lbl_ProdNombre2.setForeground(Color.BLACK);
			lbl_ProdNombre2.setBorder(null);
			lbl_ProdNombre2.setBackground(Color.BLACK);
			lbl_ProdNombre2.setBounds(235, 205, 210, 154);
			frame.getContentPane().add(lbl_ProdNombre2);

			JLabel lblPrecio2 = new JLabel(ProductoPrecio + " �");
			lblPrecio2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
			lblPrecio2.setToolTipText("precio");
			lblPrecio2.setForeground(Color.BLACK);
			lblPrecio2.setBorder(null);
			lblPrecio2.setBackground(Color.BLACK);
			lblPrecio2.setBounds(235, 250, 210, 154);
			frame.getContentPane().add(lblPrecio2);

			PrecioTotal += ProductoPrecio;
			}
			
			if(Producto3Cesta) {
				Cesta(Producto3);
			Image imgProducto3 = new ImageIcon(ProductoImg).getImage();
			ImageIcon img2Producto3 = new ImageIcon(imgProducto3.getScaledInstance(200, 200, Image.SCALE_SMOOTH));

			JLabel lbl_ProdImagen3 = new JLabel("");
			lbl_ProdImagen3.setIcon(img2Producto3);
			lbl_ProdImagen3.setToolTipText("imagen");
			lbl_ProdImagen3.setBorder(null);
			lbl_ProdImagen3.setForeground(new Color(0, 0, 0));
			lbl_ProdImagen3.setBackground(new Color(0, 0, 0));
			lbl_ProdImagen3.setBounds(25, 420, 210, 154);
			frame.getContentPane().add(lbl_ProdImagen3);

			JLabel lbl_ProdNombre3 = new JLabel(Producto3);
			lbl_ProdNombre3.setFont(new Font("Tahoma", Font.BOLD, 13));
			lbl_ProdNombre3.setToolTipText("nombre descripcion");
			lbl_ProdNombre3.setForeground(Color.BLACK);
			lbl_ProdNombre3.setBorder(null);
			lbl_ProdNombre3.setBackground(Color.BLACK);
			lbl_ProdNombre3.setBounds(235, 384, 210, 154);
			frame.getContentPane().add(lbl_ProdNombre3);

			JLabel lblPrecio3 = new JLabel(ProductoPrecio + " �");
			lblPrecio3.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
			lblPrecio3.setToolTipText("precio");
			lblPrecio3.setForeground(Color.BLACK);
			lblPrecio3.setBorder(null);
			lblPrecio3.setBackground(Color.BLACK);
			lblPrecio3.setBounds(235, 429, 210, 154);
			frame.getContentPane().add(lblPrecio3);

			PrecioTotal += ProductoPrecio;
			}
			
			JLabel CuadroProductos = new JLabel("");
			CuadroProductos.setBorder(new LineBorder(new Color(0, 0, 0)));
			CuadroProductos.setBounds(24, 61, 510, 696);
			frame.getContentPane().add(CuadroProductos);
			
			JLabel TituloCuadradoTotal = new JLabel("Informaci\u00F3n del pedido");
			TituloCuadradoTotal.setFont(new Font("Tahoma", Font.BOLD, 15));
			TituloCuadradoTotal.setBounds(678, 187, 200, 20);
			frame.getContentPane().add(TituloCuadradoTotal);		
			
			JLabel LineCuadradoTotal = new JLabel("");
			LineCuadradoTotal.setBorder(new LineBorder(new Color(0, 0, 0)));
			LineCuadradoTotal.setBounds(660, 207, 229, 2);
			frame.getContentPane().add(LineCuadradoTotal);
			
			JLabel ProductosCuadradoTotal = new JLabel("Productos: " + TotalCesta);
			ProductosCuadradoTotal.setFont(new Font("Tahoma", Font.BOLD, 13));
			ProductosCuadradoTotal.setBounds(637, 251, 155, 20);
			frame.getContentPane().add(ProductosCuadradoTotal);
			
			JLabel PrecioCuadradoTotal = new JLabel("Precio total: " + DosDecimales.format(PrecioTotal) + "�");
			PrecioCuadradoTotal.setFont(new Font("Tahoma", Font.BOLD, 13));
			PrecioCuadradoTotal.setBounds(637, 292, 241, 20);
			frame.getContentPane().add(PrecioCuadradoTotal);
			
			JLabel CuadroTotal = new JLabel("");
			CuadroTotal.setVerticalAlignment(SwingConstants.TOP);
			CuadroTotal.setBorder(new LineBorder(new Color(0, 0, 0)));
			CuadroTotal.setBounds(613, 178, 314, 188);
			frame.getContentPane().add(CuadroTotal);
			
			
		}
		
		JButton btnRealizarPedido = new JButton("Realizar pedido");
		btnRealizarPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RestaStock();
				CargarInterfaz cargar = new CargarInterfaz();
	            cargar.CargarRealizarPedido(ir2, usuario, contrasenia, TotalCesta, PrecioTotal);
                frame.setVisible(false);				//Ocultamos InterfazRegistro
			}
		});
		btnRealizarPedido.setFocusPainted(false);
		btnRealizarPedido.setForeground(Color.WHITE);
		btnRealizarPedido.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnRealizarPedido.setBorder(null);
		btnRealizarPedido.setBackground(new Color(0, 128, 0));
		btnRealizarPedido.setAutoscrolls(true);
		btnRealizarPedido.setBounds(687, 408, 176, 23);
		frame.getContentPane().add(btnRealizarPedido);
		
		JButton button_1 = new JButton("Volver al menu");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CargarInterfaz cargar = new CargarInterfaz();
				cargar.CargarListaProductos(irPrincipal, usuario, contrasenia);
				frame.setVisible(false);
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(new Color(0, 128, 0));
		button_1.setBounds(687, 732, 176, 23);
		frame.getContentPane().add(button_1);
		
		
		JLabel lblNewLabel_ProductoImg = new JLabel("New label");
		lblNewLabel_ProductoImg.setIcon(new ImageIcon("img\\Fondo1.jpg"));
		lblNewLabel_ProductoImg.setBounds(0, 0, 950, 800);
		frame.getContentPane().add(lblNewLabel_ProductoImg);
		
	}
	public void Cesta(String Producto) {
		try {
			// Conexi�n con administrador
			Class.forName("com.mysql.jdbc.Driver");
			Connection conectar = (Connection) DriverManager
					.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", "admin", "admin@dosa_2019"); //188.127.164.238

			// create the java statement
			Statement st = conectar.createStatement(); // Crear una conexi�n que luego se podra cerrar (administrador)

			/*********************************************************/			

			String Datos = "SELECT * FROM APP_Tienda.PRODUCTOS WHERE Nombre = '" + Producto + "'";

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(Datos);

			// iterate through the java resultset
			 while (rs.next()) {
					ProductoPrecio = rs.getDouble("Precio");										
					ProductoImg = rs.getString("Imagen");
			}
			Pintado++;
				
			st.close(); // Finalizar conexi�n (administrador)
		} catch (Exception e) {
			System.err.println("Ha habido un error");
			System.err.println(e.getMessage());
		}
		
	}
	
	public void RestaStock() {
		try {
			// Conexi�n con administrador
			Class.forName("com.mysql.jdbc.Driver");
			Connection conectar = (Connection) DriverManager
					.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", usuario, contrasenia); //188.127.164.238

			
			// create the java statement
			Statement st = conectar.createStatement(); // Crear una conexi�n que luego se podra cerrar (administrador)

			/*********************************************************/
			int Stock = 0;
			int NewStock = 0;
			if(Producto1Cesta) {
				String DatoStock = "SELECT Stock FROM APP_Tienda.PRODUCTOS WHERE Nombre = '" + Producto1 + "'";
				// execute the query, and get a java resultset
				ResultSet rs = st.executeQuery(DatoStock);

				// iterate through the java resultset
				 while (rs.next()) {
						Stock = rs.getInt("Stock");
				}
				
				NewStock = Stock - 1;
				
				System.out.println(NewStock);

				String Datos = "UPDATE APP_Tienda.PRODUCTOS SET Stock = " + NewStock + " WHERE Nombre = '" + Producto1 + "'";

				// execute the query, and get a java resultset
				System.out.println(NewStock);

				st.executeUpdate(Datos);
				System.out.println(NewStock);
			}
			
			if(Producto2Cesta) {
				String DatoStock = "SELECT Stock FROM APP_Tienda.PRODUCTOS WHERE Nombre = '" + Producto2 + "'";
				// execute the query, and get a java resultset
				ResultSet rs = st.executeQuery(DatoStock);

				// iterate through the java resultset
				 while (rs.next()) {
						Stock = rs.getInt("Stock");
				}
				
				NewStock = Stock - 1;

				String Datos = "UPDATE APP_Tienda.PRODUCTOS SET Stock = " + NewStock + " WHERE Nombre = '" + Producto2 + "'";

				// execute the query, and get a java resultset
				st.executeUpdate(Datos);
			}
			
			if(Producto3Cesta) {
				String DatoStock = "SELECT Stock FROM APP_Tienda.PRODUCTOS WHERE Nombre = '" + Producto3 + "'";
				// execute the query, and get a java resultset
				ResultSet rs = st.executeQuery(DatoStock);

				// iterate through the java resultset
				 while (rs.next()) {
						Stock = rs.getInt("Stock");
				}
				
				NewStock = Stock - 1;

				String Datos = "UPDATE APP_Tienda.PRODUCTOS SET Stock = " + NewStock + " WHERE Nombre = '" + Producto3 + "'";

				// execute the query, and get a java resultset
				st.executeUpdate(Datos);
			}
			
            JOptionPane.showMessageDialog(null, "�Has realizado el pedido con exito!");
				
			st.close(); // Finalizar conexi�n (administrador)
		} catch (Exception e) {
			System.err.println("Ha habido un error");
			System.err.println(e.getMessage());
		}
		
	}
	
	
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public InterfacesListaProductos getIr() {
		return ir;
	}

	public void setIr(InterfacesListaProductos ir) {
		this.ir = ir;
	}
	public int getTotalCesta() {
		return TotalCesta;
	}

	public void setTotalCesta(int totalCesta) {
		TotalCesta = totalCesta;
	}

	public String getProducto1() {
		return Producto1;
	}

	public void setProducto1(String producto1) {
		Producto1 = producto1;
	}

	public String getProducto2() {
		return Producto2;
	}

	public void setProducto2(String producto2) {
		Producto2 = producto2;
	}

	public String getProducto3() {
		return Producto3;
	}

	public void setProducto3(String producto3) {
		Producto3 = producto3;
	}

	public boolean isProducto1Cesta() {
		return Producto1Cesta;
	}

	public void setProducto1Cesta(boolean producto1Cesta) {
		Producto1Cesta = producto1Cesta;
	}

	public boolean isProducto2Cesta() {
		return Producto2Cesta;
	}

	public void setProducto2Cesta(boolean producto2Cesta) {
		Producto2Cesta = producto2Cesta;
	}

	public boolean isProducto3Cesta() {
		return Producto3Cesta;
	}

	public void setProducto3Cesta(boolean producto3Cesta) {
		Producto3Cesta = producto3Cesta;
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
}
