import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import java.awt.Toolkit;

public class InterfazRegistro {

    private JFrame frame;
    InterfazLogin irLogin;

	private JTextField textField_nombre;
    private JTextField textField_correo;
    private JPasswordField passwordField_pass;
    private JPasswordField passwordField_pass_confir;

    //Creamos variables para pasar a la InterfazVerificar
    String codigover = "";
    String correo_electronico = "";
    String password = "";
    String nombre = "";
    String apellidos = "";

    //Creamos metodo para conectar InterfazRegistro con InterfazVerificar y pasamos parametros
    InterfazVerificar verificar = new InterfazVerificar(this, codigover, correo_electronico, password, nombre, apellidos);
    private JTextField textField_apellidos;

    public InterfazRegistro(InterfazLogin irLogin) {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
        frame.setTitle("Menu Registro");
        frame.getContentPane().setForeground(Color.WHITE);
        frame.setBounds(100, 100, 866, 658);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setLocationRelativeTo(null);
        
        JLabel label = new JLabel("");
        label.setIcon(new ImageIcon("img\\Prueba2.jpg"));
        label.setBounds(253, 43, 287, 71);
        frame.getContentPane().add(label);

        JLabel lblNombre = new JLabel("Nombre");
        lblNombre.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblNombre.setBounds(249, 185, 222, 14);
        lblNombre.setForeground(new Color(0, 0, 0));
        frame.getContentPane().add(lblNombre);

        textField_nombre = new JTextField();
        textField_nombre.setBounds(249, 210, 304, 20);
        frame.getContentPane().add(textField_nombre);
        textField_nombre.setColumns(10);

        JLabel lblApellidos = new JLabel("Apellidos");
        lblApellidos.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblApellidos.setForeground(new Color(0, 0, 0));
        lblApellidos.setBounds(249, 243, 70, 14);
        frame.getContentPane().add(lblApellidos);

        textField_apellidos = new JTextField();
        textField_apellidos.setBounds(249, 268, 304, 20);
        frame.getContentPane().add(textField_apellidos);
        textField_apellidos.setColumns(10);

        JLabel lblCorreoElectrnico = new JLabel("Correo electr\u00F3nico");
        lblCorreoElectrnico.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblCorreoElectrnico.setBounds(249, 299, 222, 14);
        lblCorreoElectrnico.setForeground(new Color(0, 0, 0));
        frame.getContentPane().add(lblCorreoElectrnico);

        textField_correo = new JTextField();
        textField_correo.setBounds(249, 324, 304, 20);
        textField_correo.setColumns(10);
        frame.getContentPane().add(textField_correo);

        JLabel lblContrasea = new JLabel("Contrase\u00F1a");
        lblContrasea.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblContrasea.setBounds(249, 351, 222, 14);
        lblContrasea.setForeground(new Color(0, 0, 0));
        frame.getContentPane().add(lblContrasea);

        passwordField_pass = new JPasswordField();
        passwordField_pass.setBounds(249, 376, 304, 20);
        frame.getContentPane().add(passwordField_pass);

        JLabel lblConfirmarTuContrasea = new JLabel("Confirmar tu contrase\u00F1a");
        lblConfirmarTuContrasea.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblConfirmarTuContrasea.setBounds(249, 407, 222, 14);
        lblConfirmarTuContrasea.setForeground(new Color(0, 0, 0));
        frame.getContentPane().add(lblConfirmarTuContrasea);

        passwordField_pass_confir = new JPasswordField();
        passwordField_pass_confir.setBounds(249, 426, 304, 20);
        frame.getContentPane().add(passwordField_pass_confir);

        JButton button_8 = new JButton("Crear Cuenta");
        button_8.setFocusPainted(false);
        button_8.setForeground(Color.WHITE);
        button_8.setBackground(new Color(0, 128, 0));
        button_8.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                boolean comprueba = false;
                comprueba = Validar(comprueba);
                if(comprueba == true) {
                    verificar.getFrame().setVisible(true); 	//Si comprueba es true hacemos visible InterfazVerifica
                    frame.setVisible(false);				//Ocultamos InterfazRegistro
                }
            }
        });
        button_8.setBounds(249, 457, 304, 23);
        button_8.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
        frame.getContentPane().add(button_8);

        JButton btnIniciarSesin = new JButton("Iniciar Sesi\u00F3n");
        btnIniciarSesin.setFocusPainted(false);
        btnIniciarSesin.setForeground(Color.WHITE);
        btnIniciarSesin.setBackground(new Color(0, 128, 0));
        btnIniciarSesin.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		InterfazLogin cargar = new InterfazLogin();
				cargar.InterfazLogin();
				cargar.getFrame().setVisible(true); 
				frame.setVisible(false);
        	}
        });
        btnIniciarSesin.setBounds(249, 521, 304, 23);
        btnIniciarSesin.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
        frame.getContentPane().add(btnIniciarSesin);
        
        JLabel lblyaTienes = new JLabel("\u2014\u2014\u2014\u2014  \u00BFYa tienes cuenta en BuyMe?  \u2014\u2014\u2014\u2014");
        lblyaTienes.setForeground(Color.BLACK);
        lblyaTienes.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblyaTienes.setBounds(268, 496, 256, 14);
        frame.getContentPane().add(lblyaTienes);
        
        JLabel lblR = new JLabel("Crear Cuenta");
        lblR.setForeground(Color.BLACK);
        lblR.setFont(new Font("Microsoft YaHei", Font.BOLD, 25));
        lblR.setBackground(SystemColor.menu);
        lblR.setBounds(249, 136, 163, 34);
        frame.getContentPane().add(lblR);
        
        JTextPane textPane = new JTextPane();
        textPane.setForeground(Color.BLACK);
        textPane.setEditable(false);
        textPane.setBorder(new LineBorder(Color.BLACK));
        textPane.setBackground(Color.WHITE);
        textPane.setBounds(235, 125, 332, 460);
        frame.getContentPane().add(textPane);
    }

    public boolean Validar(boolean comprueba) {
        //Obtenemos los datos introducidos por el usuario
        nombre = textField_nombre.getText();
        apellidos = textField_apellidos.getText();
        correo_electronico = textField_correo.getText();
        password = String.valueOf(passwordField_pass.getPassword());

        //Comparamos contraseņa y confirmar contraseņa
        boolean compara = String.valueOf(passwordField_pass.getPassword()).equals(String.valueOf(passwordField_pass_confir.getPassword()));

        //Validamos contraseņa y correo
        Validar valida = new Validar();
        boolean validacorreo = valida.ValidarCorreo(correo_electronico);
        boolean validacontrasenia = valida.ValidarContrasenia(password);

        if(compara == true && validacorreo == true && validacontrasenia == true && !nombre.equals("") && !apellidos.equals("")) {
            comprueba = true;
            //Envia correo con el codigo (token)
            correo correo = new correo();
            String codigoverifica = correo.parametros(correo_electronico);

            //Insertamos datos en el metodo que se los enviara a InterfazVerifica
            verificar.setCodigover(codigoverifica);
            verificar.setCorreo_electronico(correo_electronico);
            verificar.setPassword(password);
            verificar.setNombre(nombre);
            verificar.setApellidos(apellidos);
        }
        else {
            JOptionPane.showMessageDialog(null, "Error al rellenar los campos. El nombre debe ser rellenado, las contraseņas iguales y el correo debe existir");

            //Borramos datos de todas las casillas
            textField_nombre.setText(null);
            textField_correo.setText(null);
            passwordField_pass.setText(null);
            passwordField_pass_confir.setText(null);
            textField_apellidos.setText(null);
            comprueba = false;
        }

        //Devolvemos valor comprueba para abrir, o no, la InterfazVerifica
        return comprueba;

    }
    
    public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public InterfazLogin getIrLogin() {
		return irLogin;
	}

	public void setIrLogin(InterfazLogin irLogin) {
		this.irLogin = irLogin;
	}

	public void InterfazRegistro(InterfazLogin irLogin2) {
		// TODO Auto-generated method stub
		
	}
}
