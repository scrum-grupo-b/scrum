import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;

public class InterfazDescripcionProducto {

	private JFrame frame;
	private JTextField textField;

	String NombreProducto,
		   DetallesProducto,
		   ProveedorProducto,
		   PaisOrigenProducto,
		   ImagenProducto;
	
	String usuario,
		   contrasenia;
	

	Date FechaProducto; 
	int StockProducto;
	double PrecioProducto;
	InterfacesListaProductos irListaProductos;
	InterfazPrincipal irPrincipal;
	protected String DescripcionProducto;


	public InterfazDescripcionProducto(InterfacesListaProductos irListaProductos, String NombreProducto, String usuario, String contrasenia) {
		setNombreProducto(NombreProducto);
		setUsuario(usuario);
		setContrasenia(contrasenia);
		DetallesProducto();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
        frame.setTitle("Menu Detalles");
		frame.setBounds(100, 100, 955, 832);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("Busqueda");
		label.setFont(new Font("Sylfaen", Font.BOLD, 16));
		label.setBounds(10, 12, 70, 22);
		frame.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("\uD83D\uDD0D");
		label_1.setBounds(373, 11, 24, 18);
		frame.getContentPane().add(label_1);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(90, 13, 304, 20);
		frame.getContentPane().add(textField);
		
		// Redimensiona imagen de lblNewLabel_1
		Image img = new ImageIcon(ImagenProducto).getImage();
		ImageIcon img2 = new ImageIcon(img.getScaledInstance(410, 325, Image.SCALE_SMOOTH));
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(img2);
		lblNewLabel_1.setBounds(21, 93, 406, 322);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_3 = new JLabel(" Fecha de publicaci\u00F3n: " + FechaProducto);
		lblNewLabel_3.setForeground(Color.GRAY);
		lblNewLabel_3.setBorder(null);
		lblNewLabel_3.setBounds(437, 391, 275, 24);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Pais de origen: " + PaisOrigenProducto);
		lblNewLabel_4.setBorder(null);
		lblNewLabel_4.setForeground(Color.GRAY);
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblNewLabel_4.setBounds(437, 185, 226, 34);
		frame.getContentPane().add(lblNewLabel_4);
		
		JTextArea txtrNombreDescriptivoDel = new JTextArea();
		txtrNombreDescriptivoDel.setEditable(false);
		txtrNombreDescriptivoDel.setFont(new Font("Tahoma", Font.BOLD, 25));
		txtrNombreDescriptivoDel.setOpaque(false);
		txtrNombreDescriptivoDel.setWrapStyleWord(true);
		txtrNombreDescriptivoDel.setLineWrap(true);
		txtrNombreDescriptivoDel.setText(NombreProducto);
		txtrNombreDescriptivoDel.setBorder(null);
		txtrNombreDescriptivoDel.setBounds(437, 93, 406, 94);
		frame.getContentPane().add(txtrNombreDescriptivoDel);
		
		JTextArea txtrRecibeloEnDos = new JTextArea();
		txtrRecibeloEnDos.setEditable(false);
		txtrRecibeloEnDos.setOpaque(false);
		txtrRecibeloEnDos.setBorder(null);
		txtrRecibeloEnDos.setLineWrap(true);
		txtrRecibeloEnDos.setWrapStyleWord(true);
		txtrRecibeloEnDos.setForeground(Color.GRAY);
		txtrRecibeloEnDos.setFont(new Font("Tahoma", Font.PLAIN, 12));
		txtrRecibeloEnDos.setText("Recibelo en dos dias con la garantia del proveedor: " + ProveedorProducto);
		txtrRecibeloEnDos.setBounds(437, 300, 406, 41);
		frame.getContentPane().add(txtrRecibeloEnDos);
		
		if(StockProducto == 0) {
			JLabel lblNewLabel_5 = new JLabel("NO DISPONEMOS DE STOCK DE ESTE PRODUCTO EN ESTOS MOMENTOS");
			lblNewLabel_5.setBorder(null);
			lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD, 11));
			lblNewLabel_5.setForeground(Color.RED);
			lblNewLabel_5.setBounds(437, 261, 419, 41);
			frame.getContentPane().add(lblNewLabel_5);
		}
		else {
			JLabel lblNewLabel_2 = new JLabel("Stock disponible: " + StockProducto + " unidades");
			lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
			if(StockProducto < 10)
				lblNewLabel_2.setForeground(Color.RED);
			else
				lblNewLabel_2.setForeground(Color.GRAY);
			lblNewLabel_2.setBorder(null);
			lblNewLabel_2.setBounds(437, 260, 406, 41);
			frame.getContentPane().add(lblNewLabel_2);
		}
		
		JLabel lblPrecioEtc = new JLabel(PrecioProducto + " �");
		lblPrecioEtc.setForeground(Color.BLACK);
		lblPrecioEtc.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblPrecioEtc.setBorder(null);
		lblPrecioEtc.setBounds(437, 219, 406, 41);
		frame.getContentPane().add(lblPrecioEtc);
		
		JButton buttonVolver = new JButton("Volver al menu");
		buttonVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CargarInterfaz cargar = new CargarInterfaz();
				cargar.CargarListaProductos(irPrincipal, usuario, contrasenia);
				frame.setVisible(false);
			}
		});
		
		JTextArea txtrDescripcin = new JTextArea();
		txtrDescripcin.setFont(new Font("Tahoma", Font.PLAIN, 13));
		txtrDescripcin.setOpaque(false);
		txtrDescripcin.setEditable(false);
		txtrDescripcin.setBorder(null);
		txtrDescripcin.setText(DetallesProducto);
		txtrDescripcin.setWrapStyleWord(true);
		txtrDescripcin.setLineWrap(true);
		txtrDescripcin.setBounds(21, 461, 822, 274);
		frame.getContentPane().add(txtrDescripcin);
		buttonVolver.setForeground(Color.WHITE);
		buttonVolver.setBackground(new Color(0, 128, 0));
		buttonVolver.setBounds(722, 746, 144, 23);
		frame.getContentPane().add(buttonVolver);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("img\\Fondo1.jpg"));
		lblNewLabel.setBounds(0, 0, 939, 793);
		frame.getContentPane().add(lblNewLabel);

	}
	
	public void DetallesProducto() {
		try {
			// Conexi�n con administrador
			Class.forName("com.mysql.jdbc.Driver");
			Connection conectar = (Connection) DriverManager
					.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", "anonimo", "anonimo"); //188.127.164.238

			// create the java statement
			Statement st = conectar.createStatement(); // Crear una conexi�n que luego se podra cerrar (administrador)

			/*********************************************************/
			String Datos = "SELECT * FROM APP_Tienda.PRODUCTOS WHERE Nombre = '" + NombreProducto + "'";

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(Datos);

			// iterate through the java resultset
			 while (rs.next()) {
					PrecioProducto = rs.getDouble("Precio");					
					DetallesProducto = rs.getString("Detalles");					
					StockProducto = rs.getInt("Stock");
					FechaProducto = rs.getDate("Fecha");
					ProveedorProducto = rs.getString("Proveedor");
					PaisOrigenProducto = rs.getString("PaisOrigen");
					ImagenProducto = rs.getString("Imagen");
			}
			 
				System.out.println("Dentro de DetallesProducto");

				System.out.println(PrecioProducto + " " + ImagenProducto);


			
			st.close(); // Finalizar conexi�n (administrador)
		} catch (Exception e) {
			System.err.println("Ha habido un error");
			System.err.println(e.getMessage());
		}
		
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public String getNombreProducto() {
		return NombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		NombreProducto = nombreProducto;
	}
	
	public String getDescripcionProducto() {
		return DescripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		DescripcionProducto = descripcionProducto;
	}

	public InterfacesListaProductos getIr() {
		return irListaProductos;
	}

	public void setIr(InterfacesListaProductos ir) {
		this.irListaProductos = irListaProductos;
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
}
