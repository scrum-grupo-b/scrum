import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Image;

import javax.swing.JButton;
import java.awt.Color;
import javax.swing.border.LineBorder;

public class InterfazProductoAdmin {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazProductoAdmin window = new InterfazProductoAdmin();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfazProductoAdmin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 862, 738);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblBuymeShopAdmin = new JLabel("BuyMe SHOP ADMIN PANEL");
		lblBuymeShopAdmin.setForeground(Color.BLACK);
		lblBuymeShopAdmin.setFont(new Font("Franklin Gothic Book", Font.BOLD | Font.ITALIC, 15));
		lblBuymeShopAdmin.setBounds(10, 11, 224, 18);
		frame.getContentPane().add(lblBuymeShopAdmin);

		JLabel label = new JLabel("Panel de Control:");
		label.setForeground(Color.BLACK);
		label.setFont(new Font("Franklin Gothic Book", Font.PLAIN, 15));
		label.setBounds(10, 40, 179, 24);
		frame.getContentPane().add(label);

		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setFocusPainted(false);
		btnBorrar.setForeground(Color.WHITE);
		btnBorrar.setBackground(new Color(0, 128, 0));
		btnBorrar.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnBorrar.setBounds(647, 172, 154, 23);
		frame.getContentPane().add(btnBorrar);

		JButton btnEditar = new JButton("Editar");
		btnEditar.setFocusPainted(false);
		btnEditar.setForeground(Color.WHITE);
		btnEditar.setBackground(new Color(0, 128, 0));
		btnEditar.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnEditar.setBounds(647, 138, 154, 23);
		frame.getContentPane().add(btnEditar);

		JButton button = new JButton("Editar");
		button.setFocusPainted(false);
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(0, 128, 0));
		button.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button.setBounds(647, 292, 154, 23);
		frame.getContentPane().add(button);

		JButton button_1 = new JButton("Borrar");
		button_1.setFocusPainted(false);
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(new Color(0, 128, 0));
		button_1.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button_1.setBounds(647, 326, 154, 23);
		frame.getContentPane().add(button_1);

		JButton button_2 = new JButton("Borrar");
		button_2.setFocusPainted(false);
		button_2.setForeground(Color.WHITE);
		button_2.setBackground(new Color(0, 128, 0));
		button_2.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button_2.setBounds(647, 483, 154, 23);
		frame.getContentPane().add(button_2);

		JButton button_3 = new JButton("Editar");
		button_3.setFocusPainted(false);
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(new Color(0, 128, 0));
		button_3.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button_3.setBounds(647, 449, 154, 23);
		frame.getContentPane().add(button_3);

		JButton button_4 = new JButton("Borrar");
		button_4.setFocusPainted(false);
		button_4.setForeground(Color.WHITE);
		button_4.setBackground(new Color(0, 128, 0));
		button_4.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button_4.setBounds(647, 642, 154, 23);
		frame.getContentPane().add(button_4);

		JButton button_5 = new JButton("Editar");
		button_5.setFocusPainted(false);
		button_5.setForeground(Color.WHITE);
		button_5.setBackground(new Color(0, 128, 0));
		button_5.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button_5.setBounds(647, 608, 154, 23);
		frame.getContentPane().add(button_5);

		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		lblNewLabel_1.setForeground(new Color(0, 0, 0));
		lblNewLabel_1.setBounds(10, 62, 143, 133);
		frame.getContentPane().add(lblNewLabel_1);

		JLabel label_1 = new JLabel("New label");
		label_1.setForeground(new Color(0, 0, 0));
		label_1.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_1.setBounds(10, 216, 143, 133);
		frame.getContentPane().add(label_1);

		JLabel label_2 = new JLabel("New label");
		label_2.setForeground(new Color(0, 0, 0));
		label_2.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_2.setBounds(10, 373, 143, 133);
		frame.getContentPane().add(label_2);

		JLabel label_3 = new JLabel("New label");
		label_3.setForeground(new Color(0, 0, 0));
		label_3.setBorder(new LineBorder(new Color(0, 0, 0)));
		label_3.setBounds(10, 532, 143, 133);
		frame.getContentPane().add(label_3);

		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("img\\AdminFondo.jpg"));
		lblNewLabel.setBounds(0, 0, 846, 699);
		frame.getContentPane().add(lblNewLabel);
	}
}
