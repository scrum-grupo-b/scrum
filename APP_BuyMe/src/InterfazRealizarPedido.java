import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InterfazRealizarPedido {

	private JFrame frame;
	InterfazPedidosProductos ir;
	InterfazPrincipal irPrincipal;
	String usuario,
		   contrasenia;
	
	int TotalCesta;
	double PrecioTotal;

	String UsuarioNombre,
		   UsuarioApellidos,
		   UsuarioCorreoElectronico,
		   UsuarioDireccion1,
		   UsuarioCiudad1,
		   UsuarioProvincia1,
		   UsuarioPortal1,
		   UsuarioPuerta1,
		   UsuarioCodigoPostal1,
		   UsuarioTelefono;
	
	String FormaPago = "";
	
	int UsuarioDescuento;
	

	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textFieldCheque;
	private JTextField textFieldDescuento;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public InterfazRealizarPedido(InterfazPedidosProductos ir, String usuario, String contrasenia, int TotalCesta, double PrecioTotal) {
		setUsuario(usuario);
		setContrasenia(contrasenia);
		setTotalCesta(TotalCesta);
		setPrecioTotal(PrecioTotal);
		Datos();
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
        frame.setTitle("Menu Realizar Pedido");
		frame.getContentPane().setLayout(null);
		frame.setBounds(0, 0, 950, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		
		JLabel lblBarraEspaciadora = new JLabel(
				"\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014");
		lblBarraEspaciadora.setForeground(new Color(192, 192, 192));
		lblBarraEspaciadora.setBounds(10, 76, 915, 14);
		frame.getContentPane().add(lblBarraEspaciadora);
		
		JLabel lblNewLabel = new JLabel("Tramitar pedido");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblNewLabel.setBounds(23, 11, 426, 67);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblBarraEspacio = new JLabel("");
		lblBarraEspacio.setBorder(new LineBorder(Color.LIGHT_GRAY));
		lblBarraEspacio.setBounds(20, 128, 887, 2);
		frame.getContentPane().add(lblBarraEspacio);
		
		JLabel lblTxtNombre = new JLabel("Nombre:");
		lblTxtNombre.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtNombre.setBounds(30, 141, 60, 14);
		frame.getContentPane().add(lblTxtNombre);
		
		JLabel lblNombre = new JLabel(UsuarioNombre);
		lblNombre.setBorder(null);
		lblNombre.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblNombre.setBounds(148, 141, 659, 14);
		frame.getContentPane().add(lblNombre);
		
		JLabel lblTxtDireccion = new JLabel("Direccion:");
		lblTxtDireccion.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtDireccion.setBounds(30, 167, 115, 14);
		frame.getContentPane().add(lblTxtDireccion);
		
		JLabel lblDireccion = new JLabel(UsuarioDireccion1 + ", " + UsuarioCiudad1 + ", " + UsuarioProvincia1 + ", " + UsuarioPortal1 + ", " + UsuarioPuerta1 + ", " + UsuarioCodigoPostal1);		
		lblDireccion.setBorder(null);
		lblDireccion.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDireccion.setBounds(148, 167, 659, 14);
		frame.getContentPane().add(lblDireccion);
		
		JLabel lblTxtEmail = new JLabel("Correo electronio:");
		lblTxtEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtEmail.setBounds(30, 191, 115, 14);
		frame.getContentPane().add(lblTxtEmail);
		
		JLabel lblEmail = new JLabel(UsuarioCorreoElectronico);
		lblEmail.setBorder(null);
		lblEmail.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblEmail.setBounds(148, 191, 659, 14);
		frame.getContentPane().add(lblEmail);
		
		JLabel lblTxtTelf = new JLabel("Telefono:");
		lblTxtTelf.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtTelf.setBounds(30, 215, 60, 14);
		frame.getContentPane().add(lblTxtTelf);
		
		JLabel lblTelf = new JLabel(UsuarioTelefono);
		lblTelf.setBorder(null);
		lblTelf.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblTelf.setBounds(148, 215, 659, 14);
		frame.getContentPane().add(lblTelf);
		
		JLabel lblDatosEnvio = new JLabel("  Datos de envio");
		lblDatosEnvio.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDatosEnvio.setVerticalAlignment(SwingConstants.TOP);
		lblDatosEnvio.setBackground(Color.LIGHT_GRAY);
		lblDatosEnvio.setBorder(new LineBorder(Color.GRAY, 1, true));
		lblDatosEnvio.setBounds(20, 101, 887, 163);
		frame.getContentPane().add(lblDatosEnvio);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Tarjeta de credito");
		rdbtnNewRadioButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormaPago = "Tarjeta de credito";
			}
		});
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setFocusPainted(false);
		rdbtnNewRadioButton.setBounds(99, 362, 151, 23);
		frame.getContentPane().add(rdbtnNewRadioButton);
		
		Image PagoTarjeta = new ImageIcon("img\\MetodoDePago_Tarjeta.jpg").getImage();
		ImageIcon Tarjeta = new ImageIcon(PagoTarjeta.getScaledInstance(50, 50, Image.SCALE_SMOOTH));
		
		JLabel lblPagoTarjeta = new JLabel("");
		lblPagoTarjeta.setIcon(Tarjeta);
		lblPagoTarjeta.setBorder(null);
		lblPagoTarjeta.setBounds(290, 356, 53, 36);
		frame.getContentPane().add(lblPagoTarjeta);
		
		JRadioButton rdbtnEnEfectivo = new JRadioButton("Contra reembolso");
		rdbtnEnEfectivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormaPago = "Contra reembolso";
			}
		});
		buttonGroup.add(rdbtnEnEfectivo);
		rdbtnEnEfectivo.setFocusPainted(false);
		rdbtnEnEfectivo.setBounds(99, 401, 151, 23);
		frame.getContentPane().add(rdbtnEnEfectivo);
		
		Image PagoEfectivo = new ImageIcon("img\\MetodoDePago_Efectivo.png").getImage();
		ImageIcon Efectivo = new ImageIcon(PagoEfectivo.getScaledInstance(30, 30, Image.SCALE_SMOOTH));
		
		JLabel lblPagoEfectivo = new JLabel("");
		lblPagoEfectivo.setIcon(Efectivo);
		lblPagoEfectivo.setBorder(null);
		lblPagoEfectivo.setBounds(300, 396, 43, 36);
		frame.getContentPane().add(lblPagoEfectivo);
		
		JRadioButton rdbtnPaypal = new JRadioButton("PayPal");
		rdbtnPaypal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FormaPago = "PayPal";
			}
		});
		buttonGroup.add(rdbtnPaypal);
		rdbtnPaypal.setFocusPainted(false);
		rdbtnPaypal.setBounds(99, 441, 151, 23);
		frame.getContentPane().add(rdbtnPaypal);
		
		Image PagoPayPal = new ImageIcon("img\\MetodoDePago_PayPal.jpeg").getImage();
		ImageIcon PayPal = new ImageIcon(PagoPayPal.getScaledInstance(60, 50, Image.SCALE_SMOOTH));
		
		JLabel lblPagoPayPal = new JLabel("");
		lblPagoPayPal.setIcon(PayPal);
		lblPagoPayPal.setBorder(null);
		lblPagoPayPal.setBounds(290, 436, 53, 36);
		frame.getContentPane().add(lblPagoPayPal);
		
		JLabel lblBarraEspacio2 = new JLabel("");
		lblBarraEspacio2.setBorder(new LineBorder(Color.LIGHT_GRAY));
		lblBarraEspacio2.setBounds(20, 347, 400, 2);
		frame.getContentPane().add(lblBarraEspacio2);
		
		JLabel lblMetodoPago = new JLabel("  Metodo de pago");
		lblMetodoPago.setVerticalAlignment(SwingConstants.TOP);
		lblMetodoPago.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblMetodoPago.setBorder(new LineBorder(Color.GRAY, 1, true));
		lblMetodoPago.setBackground(Color.LIGHT_GRAY);
		lblMetodoPago.setBounds(20, 320, 400, 163);
		frame.getContentPane().add(lblMetodoPago);
		
		JLabel lblTxtCheque = new JLabel("Cheque regalo:");
		lblTxtCheque.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtCheque.setBounds(496, 378, 92, 14);
		frame.getContentPane().add(lblTxtCheque);
		
		textFieldCheque = new JTextField();
		textFieldCheque.setBounds(589, 375, 307, 20);
		frame.getContentPane().add(textFieldCheque);
		textFieldCheque.setColumns(10);
		
		textFieldDescuento = new JTextField();
		textFieldDescuento.setColumns(10);
		textFieldDescuento.setBounds(627, 418, 269, 20);
		frame.getContentPane().add(textFieldDescuento);
		
		JButton btnAplicar = new JButton("Aplicar");
		btnAplicar.setForeground(Color.WHITE);
		btnAplicar.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnAplicar.setFocusPainted(false);
		btnAplicar.setBorder(null);
		btnAplicar.setBackground(new Color(0, 128, 0));
		btnAplicar.setAutoscrolls(true);
		btnAplicar.setBounds(788, 449, 108, 23);
		frame.getContentPane().add(btnAplicar);
		
		JLabel lblTxtCodigo = new JLabel("Codigo de descuento:");
		lblTxtCodigo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtCodigo.setBounds(496, 421, 128, 14);
		frame.getContentPane().add(lblTxtCodigo);
		
		JLabel lblBarraEspacio3 = new JLabel("");
		lblBarraEspacio3.setBorder(new LineBorder(Color.LIGHT_GRAY));
		lblBarraEspacio3.setBounds(485, 347, 422, 2);
		frame.getContentPane().add(lblBarraEspacio3);
		
		JLabel lblDescuentos = new JLabel("  Cheque regalo o codigo de descuento");
		lblDescuentos.setVerticalAlignment(SwingConstants.TOP);
		lblDescuentos.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDescuentos.setBorder(new LineBorder(Color.GRAY, 1, true));
		lblDescuentos.setBackground(Color.LIGHT_GRAY);
		lblDescuentos.setBounds(485, 320, 422, 163);
		frame.getContentPane().add(lblDescuentos);
		
		JLabel lblProductosTotales = new JLabel(String.valueOf(TotalCesta));
		lblProductosTotales.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblProductosTotales.setBorder(null);
		lblProductosTotales.setBounds(151, 553, 659, 14);
		frame.getContentPane().add(lblProductosTotales);
		
		JLabel lblTxtProductosTotales = new JLabel("Productos totales:");
		lblTxtProductosTotales.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtProductosTotales.setBounds(33, 553, 108, 14);
		frame.getContentPane().add(lblTxtProductosTotales);
		
		JLabel lblTxtPrecio = new JLabel("Precio:");
		lblTxtPrecio.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtPrecio.setBounds(33, 579, 115, 14);
		frame.getContentPane().add(lblTxtPrecio);
		
		JLabel lblPrecio = new JLabel(String.valueOf(PrecioTotal) + " €");
		lblPrecio.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblPrecio.setBorder(null);
		lblPrecio.setBounds(151, 579, 659, 14);
		frame.getContentPane().add(lblPrecio);
		
		JLabel lblTxtDescuentoPedido = new JLabel("Descuento:");
		lblTxtDescuentoPedido.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtDescuentoPedido.setBounds(33, 604, 115, 14);
		frame.getContentPane().add(lblTxtDescuentoPedido);
		
		JLabel lblDescuentoPedido = new JLabel("");
		lblDescuentoPedido.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblDescuentoPedido.setBorder(null);
		lblDescuentoPedido.setVisible(false);
		lblDescuentoPedido.setBounds(151, 603, 659, 14);
		frame.getContentPane().add(lblDescuentoPedido);
		
		
		JLabel lblTxtPrecioFinal = new JLabel("Precio final:");
		lblTxtPrecioFinal.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblTxtPrecioFinal.setBounds(33, 628, 102, 14);
		frame.getContentPane().add(lblTxtPrecioFinal);
		
		JLabel lblPrecioFinal = new JLabel("");
		lblPrecioFinal.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblPrecioFinal.setBorder(null);
		lblPrecioFinal.setVisible(false);
		lblPrecioFinal.setBounds(151, 628, 659, 14);
		frame.getContentPane().add(lblPrecioFinal);
		
		JLabel label = new JLabel("");
		label.setBorder(new LineBorder(Color.LIGHT_GRAY));
		label.setBounds(20, 541, 887, 2);
		frame.getContentPane().add(label);
		
		JLabel lblDatosPedido = new JLabel("  Datos del pedido");
		lblDatosPedido.setVerticalAlignment(SwingConstants.TOP);
		lblDatosPedido.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblDatosPedido.setBorder(new LineBorder(Color.GRAY, 1, true));
		lblDatosPedido.setBackground(Color.LIGHT_GRAY);
		lblDatosPedido.setBounds(20, 521, 887, 163);
		frame.getContentPane().add(lblDatosPedido);
		
		JButton buttonConfirmarPedido = new JButton("Confirmar pedido");
		buttonConfirmarPedido.setFocusPainted(false);
		buttonConfirmarPedido.setForeground(Color.WHITE);
		buttonConfirmarPedido.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		buttonConfirmarPedido.setBorder(null);
		buttonConfirmarPedido.setBackground(new Color(0, 128, 0));
		buttonConfirmarPedido.setAutoscrolls(true);
		buttonConfirmarPedido.setBounds(690, 695, 176, 23);
		frame.getContentPane().add(buttonConfirmarPedido);
		
		btnAplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Descuento();
				lblDescuentoPedido.setText(String.valueOf(UsuarioDescuento) + " €");
				lblDescuentoPedido.setVisible(true);
				lblPrecioFinal.setText(String.valueOf(PrecioTotal - UsuarioDescuento) + " €");
				lblPrecioFinal.setVisible(true);
			}
		});
		
		buttonConfirmarPedido.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(FormaPago.equals(""))
					JOptionPane.showMessageDialog(null, "Faltan datos por introducir");
				else {
					JOptionPane.showMessageDialog(null, "Has realizado el pedido con exito!");
					correo correo = new correo();
		            correo.parametrosPedido(UsuarioCorreoElectronico, UsuarioNombre, UsuarioApellidos, UsuarioDireccion1, UsuarioCiudad1, UsuarioProvincia1, UsuarioPortal1, UsuarioPuerta1, UsuarioCodigoPostal1, TotalCesta, PrecioTotal);
					CargarInterfaz cargar = new CargarInterfaz();
					cargar.CargarListaProductos(irPrincipal, usuario, contrasenia);
					frame.setVisible(false);
				}
			}
		});
	}
	
	public void Datos() {
		try {
			// Conexión con administrador
			Class.forName("com.mysql.jdbc.Driver");
			Connection conectar = (Connection) DriverManager
					.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", "admin", "admin@dosa_2019"); //188.127.164.238

			// create the java statement
			Statement st = conectar.createStatement(); // Crear una conexión que luego se podra cerrar (administrador)
			
			String query = "SELECT Nombre, Apellidos, CorreoElectronico, Direccion1, Ciudad1, Provincia1, Portal1, Puerta1, CodigoPostal1, Telefono FROM " + usuario + "_view";

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);

			// iterate through the java resultset
			 while (rs.next()) {
				 UsuarioNombre = rs.getString("Nombre");
				 UsuarioApellidos = rs.getString("Apellidos");					
				 UsuarioCorreoElectronico = rs.getString("CorreoElectronico");					
				 UsuarioDireccion1 = rs.getString("Direccion1");					
				 UsuarioCiudad1 = rs.getString("Ciudad1");					
				 UsuarioProvincia1 = rs.getString("Provincia1");					
				 UsuarioPortal1 = rs.getString("Portal1");					
				 UsuarioPuerta1 = rs.getString("Puerta1");					
				 UsuarioCodigoPostal1 = rs.getString("CodigoPostal1");					
				 UsuarioTelefono = rs.getString("Telefono");						
			}			
			 
			 st.close();
		}catch (Exception e) {
			System.err.println("Ha habido un error");
			System.err.println(e.getMessage());
		}
	}
	
	public void Descuento() {
		String Descuento = textFieldDescuento.getText();
		try {
			// Conexión con administrador
			Class.forName("com.mysql.jdbc.Driver");
			Connection conectar = (Connection) DriverManager
					.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", usuario, contrasenia); //188.127.164.238

			// create the java statement
			Statement st = conectar.createStatement(); // Crear una conexión que luego se podra cerrar (administrador)
			
			String query = "SELECT Descuento FROM DESCUENTOS WHERE Nombre = '" + Descuento + "'";

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);

			// iterate through the java resultset
			 while (rs.next()) {
				 UsuarioDescuento = rs.getInt("Descuento");						
			}			
			 
			 st.close();
		}catch (Exception e) {
			System.err.println("Ha habido un error");
			System.err.println(e.getMessage());
		}
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public InterfazPedidosProductos getIr() {
		return ir;
	}

	public void setIr(InterfazPedidosProductos ir) {
		this.ir = ir;
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String contrasenia) {
		this.contrasenia = contrasenia;
	}
	
	public int getTotalCesta() {
		return TotalCesta;
	}

	public void setTotalCesta(int totalCesta) {
		TotalCesta = totalCesta;
	}

	public double getPrecioTotal() {
		return PrecioTotal;
	}

	public void setPrecioTotal(double precioTotal) {
		PrecioTotal = precioTotal;
	}
}
