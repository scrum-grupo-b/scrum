import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;

public class InterfazPrincipal {

	JFrame frmMenuInicio;
	InterfazPrincipal irPrincipal;
	String usuario = "anonimo",
		   contrasenia = "anonimo";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazPrincipal window = new InterfazPrincipal();
					window.frmMenuInicio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfazPrincipal() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMenuInicio = new JFrame();
		frmMenuInicio.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
		frmMenuInicio.setTitle("Menu Inicio");
		frmMenuInicio.getContentPane().setBackground(Color.WHITE);
		frmMenuInicio.getContentPane().setForeground(Color.WHITE);
		frmMenuInicio.setResizable(false);
		frmMenuInicio.setBounds(100, 100, 860, 724);
		frmMenuInicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMenuInicio.getContentPane().setLayout(null);
		frmMenuInicio.setLocationRelativeTo(null);

		JLabel label = new JLabel("");
		label.setBounds(220, 452, 46, 14);
		frmMenuInicio.getContentPane().add(label);

		JButton btnNewButton = new JButton("Identificate");
		btnNewButton.setFocusPainted(false);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(new Color(0, 128, 0));

		btnNewButton.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnNewButton.setBounds(220, 344, 398, 32);
		frmMenuInicio.getContentPane().add(btnNewButton);

		JButton btnAccesoTienda = new JButton("Acceso Tienda");
		btnAccesoTienda.setFocusPainted(false);
		btnAccesoTienda.setBackground(new Color(0, 128, 0));
		btnAccesoTienda.setForeground(Color.WHITE);
		btnAccesoTienda.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnAccesoTienda.setBounds(220, 300, 398, 32);
		frmMenuInicio.getContentPane().add(btnAccesoTienda);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("img\\Prueba3.jpg"));
		lblNewLabel.setBounds(104, 33, 658, 201);
		frmMenuInicio.getContentPane().add(lblNewLabel);

		JLabel label_1 = new JLabel("BuyME.es");
		label_1.setFont(new Font("Lucida Fax", Font.BOLD, 11));
		label_1.setBounds(407, 670, 60, 14);
		frmMenuInicio.getContentPane().add(label_1);

		// eventos
		btnNewButton.addActionListener(new ActionListener() {
			// navegar a la interfaz de login
			public void actionPerformed(ActionEvent arg0) {
				CargarInterfaz cargar = new CargarInterfaz();
				cargar.CargarLogin(irPrincipal);
				frmMenuInicio.setVisible(false);
			}
		});

		btnAccesoTienda.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CargarInterfaz cargar = new CargarInterfaz();
				cargar.CargarListaProductos(irPrincipal, usuario, contrasenia);
				frmMenuInicio.setVisible(false);
			}
		});

	}
}
