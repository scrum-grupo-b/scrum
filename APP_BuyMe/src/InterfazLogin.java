import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JTextField;


import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;

import javax.swing.JPasswordField;
import javax.swing.JButton;
import javax.swing.JDialog;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;


public class InterfazLogin extends JDialog{

    private JFrame frame;
    private JTextField textField;
    private JPasswordField passwordField;
    InterfazPrincipal irPrincipal;
    InterfazLogin irLogin;
    boolean resultado = false;
    String pass;
    String usuario;


    public InterfazLogin() {
		initialize();
	}
    
	/**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
        frame.setTitle("Menu Login");
        frame.setBounds(100, 100, 861, 660);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);

        JLabel lblNewLabel_1 = new JLabel("Iniciar Sesi\u00F3n");
        lblNewLabel_1.setFont(new Font("Microsoft YaHei", Font.BOLD, 25));
        lblNewLabel_1.setBackground(new Color(240, 240, 240));
        lblNewLabel_1.setForeground(Color.BLACK);
        lblNewLabel_1.setBounds(269, 150, 164, 34);
        frame.getContentPane().add(lblNewLabel_1);

        JLabel lblDireccionDeEmail = new JLabel("Nombre usuario");
        lblDireccionDeEmail.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblDireccionDeEmail.setForeground(Color.BLACK);
        lblDireccionDeEmail.setBounds(269, 195, 222, 14);
        frame.getContentPane().add(lblDireccionDeEmail);

        textField = new JTextField();
        textField.setBounds(269, 217, 260, 20);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        JLabel lblContrasea = new JLabel("Contrase\u00F1a");
        lblContrasea.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblContrasea.setForeground(Color.BLACK);
        lblContrasea.setBounds(269, 249, 222, 14);
        frame.getContentPane().add(lblContrasea);

        passwordField = new JPasswordField();
        passwordField.addKeyListener(new KeyAdapter() {
        	@Override
        	public void keyPressed(KeyEvent e) {
        		if (e.getKeyCode() == KeyEvent.VK_ENTER)
        			resultado = ValidarUsuario();
        			if(resultado) {
        				CargarInterfaz cargar = new CargarInterfaz();
    					cargar.CargarListaProductos(irPrincipal, usuario, pass);
    					frame.setVisible(false);
        			}
        	}
        });
        passwordField.setEchoChar('*');
        passwordField.setBounds(269, 274, 260, 20);
        frame.getContentPane().add(passwordField);

        JButton btnIniciarSesin = new JButton("Iniciar sesi\u00F3n");
        btnIniciarSesin.setAutoscrolls(true);
        btnIniciarSesin.setForeground(Color.WHITE);
        btnIniciarSesin.setBorder(null);
        btnIniciarSesin.setBackground(new Color(0, 128, 0));
        btnIniciarSesin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                resultado = ValidarUsuario();
    			if(resultado) {
    				CargarInterfaz cargar = new CargarInterfaz();
					cargar.CargarListaProductos(irPrincipal, usuario, pass);
					frame.setVisible(false);
    			}
         
            }
        });
        btnIniciarSesin.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
        btnIniciarSesin.setBounds(269, 351, 260, 23);
        frame.getContentPane().add(btnIniciarSesin);

        JLabel lblAlContinuarAceptas = new JLabel("Pulsa aqu\u00ED para entrar como invitado");
        lblAlContinuarAceptas.setFont(new Font("Tahoma", Font.BOLD, 10));
        lblAlContinuarAceptas.setHorizontalAlignment(SwingConstants.CENTER);
        lblAlContinuarAceptas.setForeground(Color.BLACK);
        lblAlContinuarAceptas.setBounds(292, 311, 213, 14);
        frame.getContentPane().add(lblAlContinuarAceptas);

        JButton btnCrearCuenta = new JButton("Crear Cuenta");
        btnCrearCuenta.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		InterfazRegistro cargar = new InterfazRegistro(irLogin);
				cargar.InterfazRegistro(irLogin);
				cargar.getFrame().setVisible(true); 
				frame.setVisible(false);
        	}
        });
        btnCrearCuenta.setFocusPainted(false);
        btnCrearCuenta.setForeground(Color.WHITE);
        btnCrearCuenta.setBackground(new Color(0, 128, 0));
        btnCrearCuenta.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
        btnCrearCuenta.setBounds(269, 433, 260, 23);
        frame.getContentPane().add(btnCrearCuenta);
        
        JLabel lblEresNuevo = new JLabel("\u2014\u2014\u2014\u2014  \u00BFEres nuevo en BuyMe?  \u2014\u2014\u2014\u2014");
        lblEresNuevo.setForeground(Color.BLACK);
        lblEresNuevo.setFont(new Font("Tahoma", Font.BOLD, 11));
        lblEresNuevo.setBounds(290, 408, 224, 14);
        frame.getContentPane().add(lblEresNuevo);
        
        JTextPane textPane = new JTextPane();
        textPane.setBorder(new LineBorder(Color.BLACK));
        textPane.setEditable(false);
        textPane.setForeground(Color.BLACK);
        textPane.setBackground(Color.WHITE);
        textPane.setBounds(254, 140, 286, 349);
        frame.getContentPane().add(textPane);
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon("img\\Prueba2.jpg"));
        lblNewLabel.setBounds(254, 60, 287, 71);
        frame.getContentPane().add(lblNewLabel);
        
        JLabel imagen = new JLabel("");
        imagen.setIcon(new ImageIcon("img\\Logotipo.png"));
    }
    public boolean ValidarUsuario() {
        // convierte el campo de contraseņa a texto (lee)
        pass= String.valueOf(passwordField.getPassword());
        usuario = textField.getText();
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conectar = (Connection) DriverManager.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", usuario, pass);  //188.127.164.238
            resultado = true;
        }catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Error al establecer la conexion!");
            e.printStackTrace();
        }
		return resultado;

    }
    
    public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public InterfazPrincipal getIrPrincipal() {
		return irPrincipal;
	}

	public void setIrPrincipal(InterfazPrincipal irPrincipal) {
		this.irPrincipal = irPrincipal;
	}

	public InterfazLogin(InterfazPrincipal irPrincipal) {
        initialize();
    }

	public void InterfazLogin() {
		// TODO Auto-generated method stub
		
	}
}

