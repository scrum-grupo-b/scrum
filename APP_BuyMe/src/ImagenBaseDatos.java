
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;


public class ImagenBaseDatos {
    Connection conexion;
    Statement st;
    
    public ImagenBaseDatos(String host,String user, String pass){
        try {
        	Class.forName("com.mysql.jdbc.Driver");
            conexion = (Connection) DriverManager.getConnection("jdbc:mysql://188.127.164.238:3306", "admin", "admin@dosa_2019");; 
            st = conexion.createStatement();
        } catch (Exception ex) {
            Logger.getLogger(ImagenBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean guardarImagen(String ruta,String nombre){
        String insert = "insert into Imagenes(imagen,nombre) values(?,?)";
        FileInputStream fis = null;
        PreparedStatement ps = null;
        try {
            conexion.setAutoCommit(false);
            File file = new File(ruta);
            fis = new FileInputStream(file);
            ps = conexion.prepareStatement(insert);
            ps.setBinaryStream(1,fis,(int)file.length());
            ps.setString(2, nombre);
            ps.executeUpdate();
            conexion.commit();
            return true;
        } catch (Exception ex) {
            Logger.getLogger(ImagenBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                ps.close();
                fis.close();
            } catch (Exception ex) {
                Logger.getLogger(ImagenBaseDatos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }        
        return false;
    }

        
}