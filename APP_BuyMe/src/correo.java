
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;

public class correo {

    public String parametros(String destinatario) {

        //	String destinatario =  "usuariodestinatario@gmail.com"; //A quien le quieres escribir.
        String asunto = "Correo de prueba enviado desde Java";
        GenerarCodigo codigo = new GenerarCodigo();
        String codigoverifica = codigo.Codigo();

        String cuerpo = "Esta es una prueba de correo... Inserta este codigo"
            + "para verificar la cuenta " + codigoverifica;

        enviarConGMail(destinatario, asunto, cuerpo);
        return codigoverifica;
    }

    private static void enviarConGMail(String destinatario, String asunto, String cuerpo) {
        // Esto es lo que va delante de @gmail.com en tu cuenta de correo. Es el remitente tambi�n.
        String remitente = "soportebuyme@gmail.com";  //Para la direcci�n nomcuenta@gmail.com
        String clave = "Buyme@dosa_2019";

        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", "miClaveDeGMail");    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticaci�n mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));  //Se podr�an a�adir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, clave);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();   //Si se produce un error
        }
    }
    
    public String parametrosPedido(String destinatario, String UsuarioNombre, String UsuarioApellidos, String UsuarioDireccion1, String UsuarioCiudad1, String UsuarioProvincia1, String UsuarioPortal1, String UsuarioPuerta1, String UsuarioCodigoPostal1, int TotalCesta, double Preciototal) {

        //	String destinatario =  "usuariodestinatario@gmail.com"; //A quien le quieres escribir.
        String asunto = "Correo de prueba enviado desde Java";
        GenerarCodigo codigo = new GenerarCodigo();
        String codigoverifica = codigo.Codigo();

        String cuerpo = "Hola " + UsuarioNombre + " " + UsuarioApellidos + "\n Gracias por realizar tu pedido. "
        		+ "\nTu direccion de envio es " + UsuarioDireccion1 + ", " + UsuarioCiudad1 + ", " + UsuarioProvincia1 + ", " + UsuarioPortal1 + ", " + UsuarioPuerta1 + ", " + UsuarioCodigoPostal1 +"\n"
        		+ "El pedido tiene " + TotalCesta + " producto y su precio es de " + Preciototal + " �";

        enviarConGMailPedido(destinatario, asunto, cuerpo);
        return codigoverifica;
    }

    private static void enviarConGMailPedido(String destinatario, String asunto, String cuerpo) {
        // Esto es lo que va delante de @gmail.com en tu cuenta de correo. Es el remitente tambi�n.
        String remitente = "soportebuyme@gmail.com";  //Para la direcci�n nomcuenta@gmail.com
        String clave = "Buyme@dosa_2019";

        Properties props = System.getProperties();
        props.put("mail.smtp.host", "smtp.gmail.com");  //El servidor SMTP de Google
        props.put("mail.smtp.user", remitente);
        props.put("mail.smtp.clave", "miClaveDeGMail");    //La clave de la cuenta
        props.put("mail.smtp.auth", "true");    //Usar autenticaci�n mediante usuario y clave
        props.put("mail.smtp.starttls.enable", "true"); //Para conectar de manera segura al servidor SMTP
        props.put("mail.smtp.port", "587"); //El puerto SMTP seguro de Google

        Session session = Session.getDefaultInstance(props);
        MimeMessage message = new MimeMessage(session);

        try {
            message.setFrom(new InternetAddress(remitente));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));  //Se podr�an a�adir varios de la misma manera
            message.setSubject(asunto);
            message.setText(cuerpo);
            Transport transport = session.getTransport("smtp");
            transport.connect("smtp.gmail.com", remitente, clave);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
        }
        catch (MessagingException me) {
            me.printStackTrace();   //Si se produce un error
        }
    }

}
