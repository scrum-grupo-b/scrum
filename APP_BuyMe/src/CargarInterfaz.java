
public class CargarInterfaz {
	public void CargarDescripcionProducto(InterfacesListaProductos ir, String DescripcionProducto, String usuario, String contrasenia) {
		InterfazDescripcionProducto verificar = new InterfazDescripcionProducto(ir, DescripcionProducto, usuario, contrasenia);
		verificar.setNombreProducto(DescripcionProducto);
        verificar.getFrame().setVisible(true); 	//Si comprueba es true hacemos visible InterfazVerifica
	}
	public void CargarPedidoProducto(InterfacesListaProductos ir, String Producto1, String Producto2, String Producto3, boolean Producto1Cesta, boolean Producto2Cesta, boolean Producto3Cesta, 
									int TotalCesta, String usuario, String contrasenia) {
		InterfazPedidosProductos verificar = new InterfazPedidosProductos(ir, Producto1, Producto2, Producto3, Producto1Cesta, Producto2Cesta, Producto3Cesta, TotalCesta, usuario, contrasenia);
        verificar.getFrame().setVisible(true); 	//Si comprueba es true hacemos visible InterfazVerifica
	}
	
	public void CargarRealizarPedido(InterfazPedidosProductos ir, String usuario, String contrasenia, int TotalCesta, double PrecioTotal) {
		InterfazRealizarPedido verificar = new InterfazRealizarPedido(ir, usuario, contrasenia, TotalCesta, PrecioTotal);
        verificar.getFrame().setVisible(true); 	//Si comprueba es true hacemos visible InterfazVerifica
	}
	
	public void CargarLogin(InterfazPrincipal irPrincipal) {
		InterfazLogin verificar = new InterfazLogin(irPrincipal);
        verificar.getFrame().setVisible(true); 	//Si comprueba es true hacemos visible InterfazVerifica
	}
	
	public void CargarListaProductos(InterfazPrincipal irPrincipal, String usuario, String contrasenia) {
		InterfacesListaProductos verificar = new InterfacesListaProductos(irPrincipal, usuario, contrasenia);
        verificar.getFrame().setVisible(true); 	//Si comprueba es true hacemos visible InterfazVerifica
	}
	
}
