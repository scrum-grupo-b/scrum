import java.security.SecureRandom;
import java.math.BigInteger;

public class GenerarCodigo {

    public String Codigo() {
        SecureRandom random = new SecureRandom();
        String text = new BigInteger(130, random).toString(32);
        return text;
    }

}
