import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.sql.*;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.AbstractListModel;
import javax.swing.ListSelectionModel;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

public class InterfazProductos {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazProductos window = new InterfazProductos();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the application.
	 */
	public InterfazProductos() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 860, 712);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("www.buyme.es");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Source Sans Pro Light", Font.BOLD | Font.ITALIC, 11));
		label.setBounds(382, 659, 80, 14);
		frame.getContentPane().add(label);
		
		JComboBox categorias = new JComboBox();
		categorias.setModel(new DefaultComboBoxModel(new String[] {". . ."}));
		categorias.setFont(new Font("Source Sans Pro Black", Font.BOLD, 11));
		categorias.setToolTipText("");
		categorias.setBounds(10, 65, 111, 20);
		frame.getContentPane().add(categorias);
		
		JLabel lblCategoria = new JLabel("Categorias");
		lblCategoria.setForeground(Color.BLACK);
		lblCategoria.setFont(new Font("Microsoft JhengHei", Font.BOLD, 12));
		lblCategoria.setBounds(10, 39, 73, 20);
		frame.getContentPane().add(lblCategoria);
		
		JLabel label_1 = new JLabel("");
		label_1.setIcon(new ImageIcon("img\\LogoSimple.jpg"));
		label_1.setBounds(229, 202, 392, 364);
		frame.getContentPane().add(label_1);
		
		JButton button = new JButton("Iniciar sesi\u00F3n");
		button.setFocusPainted(false);
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button.setBorder(null);
		button.setBackground(new Color(0, 128, 0));
		button.setAutoscrolls(true);
		button.setBounds(567, 5, 176, 23);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("Cesta");
		button_1.setFocusPainted(false);
		button_1.setForeground(Color.WHITE);
		button_1.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		button_1.setBackground(new Color(0, 128, 0));
		button_1.setBounds(753, 5, 81, 23);
		frame.getContentPane().add(button_1);
		
		JLabel label_2 = new JLabel("Busqueda");
		label_2.setFont(new Font("Sylfaen", Font.BOLD, 16));
		label_2.setBounds(10, 6, 70, 22);
		frame.getContentPane().add(label_2);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(90, 7, 304, 20);
		frame.getContentPane().add(textField);
		
		JLabel label_3 = new JLabel("\uD83D\uDD0D");
		label_3.setBounds(381, 4, 11, 14);
		frame.getContentPane().add(label_3);
		
		JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setIcon(new ImageIcon("img\\Fondo1.jpg"));
		lblNewLabel.setBounds(0, 0, 844, 673);
		frame.getContentPane().add(lblNewLabel);
		
		//Conexion a la base de datos para las categorias
		try {
			Connection miConexion=(Connection) DriverManager.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", "admin", "admin@dosa_2019");
			
			Statement miconsulta = (Statement) miConexion.createStatement();
			
			String consulta="SELECT DISTINCT Categoria FROM PRODUCTOS";
			
			ResultSet rs=miconsulta.executeQuery(consulta);
			
			while (rs.next()) {
				categorias.addItem(rs.getString(1));
			}
			rs.close();
		}catch(Exception e) {
		    JOptionPane.showMessageDialog(null, "Error al establecer la conexion!");
            e.printStackTrace();
		}
		
		
	}
}
