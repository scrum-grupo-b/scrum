import javax.swing.JFrame;
import java.awt.Color;
import java.awt.Cursor;

import javax.swing.JLabel;

import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import java.awt.Image;
import java.awt.Toolkit;
import java.sql.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class InterfacesListaProductos {

	private JFrame frame;
	private JTextField textField;
	InterfacesListaProductos irListaProductos;

	InterfazPrincipal irPrincipal;
	String usuario,
		   contrasenia;

	int TotalProducto = 0,
		TotalCesta = 0;
	String ProductoBuscar,
		   Producto1,
		   Producto2,
		   Producto3,
		   DescripcionProducto;
	boolean Producto1Cesta = false,
			Producto2Cesta = false,
			Producto3Cesta = false;
	boolean buscar = false;
	String ProductoImg;
	String ProductoNombre;
	String ProductoPrecio;
	int ProductoStock = 0;
	int Posicion = 0,
		Ventana = 3;
	int i = 0;
	
	
	public InterfacesListaProductos(InterfazPrincipal irVerificar, String usuario, String contrasenia) {
		setUsuario(usuario);
		setContrasenia(contrasenia);
		ListaProductos(0);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
        frame.setTitle("Menu Productos");
		frame.getContentPane().setLayout(null);
		frame.setLocationRelativeTo(null);
		

		JLabel label = new JLabel("\uD83D\uDD0D");
		label.setBounds(372, 4, 52, 41);
		frame.getContentPane().add(label);

		JLabel lblBusqueda = new JLabel("Busqueda");
		lblBusqueda.setFont(new Font("Sylfaen", Font.BOLD, 16));
		lblBusqueda.setBounds(10, 11, 70, 22);
		frame.getContentPane().add(lblBusqueda);

		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER)
					ListaProductos(0);
			}
		});
		textField.setBounds(90, 12, 304, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		if (usuario == "anonimo") {
			JButton button = new JButton("Iniciar sesi\u00F3n");
			button.setForeground(Color.WHITE);
			button.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					CargarInterfaz cargar = new CargarInterfaz();
					cargar.CargarLogin(irPrincipal);
					frame.setVisible(false);
				}
			});
			button.setFocusPainted(false);
			button.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
			button.setBorder(null);
			button.setBackground(new Color(0, 128, 0));
			button.setAutoscrolls(true);
			button.setBounds(658, 10, 176, 23);
			frame.getContentPane().add(button);
		} else {
			JLabel lblHola = new JLabel("Hola, " + usuario + "!");
			lblHola.setFont(new Font("Sylfaen", Font.BOLD, 16));
			lblHola.setBounds(658, 10, 176, 23);
			frame.getContentPane().add(lblHola);
		}
		
		JButton btnMiCestita = new JButton("Cesta");
		btnMiCestita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CargarInterfaz cargar = new CargarInterfaz();
	            cargar.CargarPedidoProducto(irListaProductos, Producto1, Producto2, Producto3, Producto1Cesta, Producto2Cesta, Producto3Cesta, TotalCesta, usuario, contrasenia);
                frame.setVisible(false);				//Ocultamos InterfazRegistro
			}
		});
		btnMiCestita.setForeground(Color.WHITE);
		btnMiCestita.setFocusPainted(false);
		btnMiCestita.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnMiCestita.setBackground(new Color(0, 128, 0));
		btnMiCestita.setBounds(844, 10, 81, 23);
		frame.getContentPane().add(btnMiCestita);
		
		if(usuario == "anonimo") 
			btnMiCestita.setEnabled(false);
		
		JButton btnAn = new JButton("Anterior");
		btnAn.setForeground(Color.WHITE);
		btnAn.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnAn.setFocusPainted(false);
		btnAn.setBorder(null);
		btnAn.setBackground(new Color(0, 128, 0));
		btnAn.setAutoscrolls(true);
		btnAn.setBounds(278, 752, 176, 23);
		frame.getContentPane().add(btnAn);
		
		JButton btnSiguiente = new JButton("Siguiente");
		btnSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListaProductos(Ventana);
				initialize();
			}
		});
		btnSiguiente.setForeground(Color.WHITE);
		btnSiguiente.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
		btnSiguiente.setFocusPainted(false);
		btnSiguiente.setBorder(null);
		btnSiguiente.setBackground(new Color(0, 128, 0));
		btnSiguiente.setAutoscrolls(true);
		btnSiguiente.setBounds(485, 752, 176, 23);
		frame.getContentPane().add(btnSiguiente);

		JLabel label_1 = new JLabel("BuyME.es");
		label_1.setFont(new Font("Lucida Fax", Font.BOLD, 11));
		label_1.setBounds(446, 785, 60, 14);
		frame.getContentPane().add(label_1);

		if (TotalProducto == 0) {
			JLabel lblNewLabel_TituloCestaVacia = new JLabel("No hemos podido encontrar el producto que busca");
			lblNewLabel_TituloCestaVacia.setBorder(null);
			lblNewLabel_TituloCestaVacia.setFont(new Font("Times New Roman", Font.BOLD, 20));
			lblNewLabel_TituloCestaVacia.setBounds(20, 71, 500, 29);
			frame.getContentPane().add(lblNewLabel_TituloCestaVacia);

			JLabel lblNewLabel_TextoCestaVacia = new JLabel("Revisa la ortografía o usa terminos más generales");
			lblNewLabel_TextoCestaVacia.setFont(new Font("Tahoma", Font.PLAIN, 13));
			lblNewLabel_TextoCestaVacia.setBounds(20, 196, 692, 29);
			frame.getContentPane().add(lblNewLabel_TextoCestaVacia);
			
		} else {
			JButton ButtonDetalleProducto1 = new JButton("");
			ButtonDetalleProducto1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
		            DescripcionProducto = Producto1;
		            CargarInterfaz cargar = new CargarInterfaz();
		            cargar.CargarDescripcionProducto(irListaProductos, DescripcionProducto, usuario, contrasenia);
                    frame.setVisible(false);				//Ocultamos InterfazRegistro
				}
			});
			ButtonDetalleProducto1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			ButtonDetalleProducto1.setContentAreaFilled(false);
			ButtonDetalleProducto1.setIcon(null);
			ButtonDetalleProducto1.setBorder(null);
			ButtonDetalleProducto1.setBounds(20, 54, 685, 193);
			frame.getContentPane().add(ButtonDetalleProducto1);
			frame.setBounds(100, 100, 961, 838);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			JLabel lbl_ProdNombre1 = new JLabel(ProductoNombre);
			lbl_ProdNombre1.setFont(new Font("Tahoma", Font.BOLD, 25));
			lbl_ProdNombre1.setToolTipText("nombre descripcion");
			lbl_ProdNombre1.setForeground(Color.BLACK);
			lbl_ProdNombre1.setBorder(null);
			lbl_ProdNombre1.setBackground(Color.BLACK);
			lbl_ProdNombre1.setBounds(231, Posicion + 75, 390, 50);
			Producto1 = lbl_ProdNombre1.getText();
			frame.getContentPane().add(lbl_ProdNombre1);
			
			// Redimensiona imagen de lblNewLabel_1
			Image img = new ImageIcon(ProductoImg).getImage();
			ImageIcon img2 = new ImageIcon(img.getScaledInstance(200, 200, Image.SCALE_SMOOTH));
			
			JLabel lbl_ProdImagen1 = new JLabel("");
			lbl_ProdImagen1.setIcon(img2);
			lbl_ProdImagen1.setToolTipText("imagen");
			lbl_ProdImagen1.setBorder(null);
			lbl_ProdImagen1.setForeground(new Color(0, 0, 0));
			lbl_ProdImagen1.setBackground(new Color(0, 0, 0));
			lbl_ProdImagen1.setBounds(21, Posicion + 55, 200, 195);
			frame.getContentPane().add(lbl_ProdImagen1);
			
			JButton btnAñadirCesta1 = new JButton("A\u00F1adir a la cesta");
			btnAñadirCesta1.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Añadir(Producto1, 1);
				}
			});
			btnAñadirCesta1.setForeground(Color.WHITE);
			if (ProductoStock == 0)
				btnAñadirCesta1.setEnabled(false);
			btnAñadirCesta1.setForeground(Color.WHITE);
			btnAñadirCesta1.setFocusPainted(false);
			btnAñadirCesta1.setBackground(new Color(0, 128, 0));
			btnAñadirCesta1.setBounds(790, Posicion + 239, 135, 23);
			frame.getContentPane().add(btnAñadirCesta1);
			
			if(usuario == "anonimo") 
				btnAñadirCesta1.setEnabled(false);
			
			JLabel lblPrecio1 = new JLabel(ProductoPrecio + " €");
			lblPrecio1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
			lblPrecio1.setToolTipText("precio");
			lblPrecio1.setForeground(Color.BLACK);
			lblPrecio1.setBorder(null);
			lblPrecio1.setBackground(Color.BLACK);
			lblPrecio1.setBounds(241, Posicion + 124, 113, 39);
			frame.getContentPane().add(lblPrecio1);

			JLabel labelStock1 = new JLabel("Stock disponible: " + ProductoStock);
			labelStock1.setToolTipText("precio");
			if(ProductoStock <= 10)
				labelStock1.setForeground(Color.RED);
			else
				labelStock1.setForeground(Color.GRAY);
			if(ProductoStock == 0)
				labelStock1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
			else
				labelStock1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
			labelStock1.setBorder(null);
			labelStock1.setBackground(Color.WHITE);
			labelStock1.setBounds(241, Posicion + 150, 161, 32);
			frame.getContentPane().add(labelStock1);
			
			ListaProductos(1);
			
			JButton ButtonDetalleProducto2 = new JButton("");
			ButtonDetalleProducto2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
		            DescripcionProducto = Producto2;
		            CargarInterfaz cargar = new CargarInterfaz();
		            cargar.CargarDescripcionProducto(irListaProductos, DescripcionProducto, usuario, contrasenia);
                    frame.setVisible(false);				//Ocultamos InterfazRegistro
				}
			});
			ButtonDetalleProducto2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			ButtonDetalleProducto2.setContentAreaFilled(false);
			ButtonDetalleProducto2.setIcon(null);
			ButtonDetalleProducto2.setBorder(null);
			ButtonDetalleProducto2.setBounds(20, 230 +54, 685, 193);
			frame.getContentPane().add(ButtonDetalleProducto2);
			frame.setBounds(100, 100, 961, 838);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			JLabel lbl_ProdNombre2 = new JLabel(ProductoNombre);
			lbl_ProdNombre2.setFont(new Font("Tahoma", Font.BOLD, 25));
			lbl_ProdNombre2.setToolTipText("nombre descripcion");
			lbl_ProdNombre2.setForeground(Color.BLACK);
			lbl_ProdNombre2.setBorder(null);
			lbl_ProdNombre2.setBackground(Color.BLACK);
			lbl_ProdNombre2.setBounds(231, 230 +Posicion + 75, 390, 50);
			Producto2 = lbl_ProdNombre2.getText();
			frame.getContentPane().add(lbl_ProdNombre2);
			
			// Redimensiona imagen de lblNewLabel_1
			Image Producto2img = new ImageIcon(ProductoImg).getImage();
			ImageIcon Producto2img1 = new ImageIcon(Producto2img.getScaledInstance(200, 200, Image.SCALE_SMOOTH));
			
			JLabel lbl_ProdImagen2 = new JLabel("");
			lbl_ProdImagen2.setIcon(Producto2img1);
			lbl_ProdImagen2.setToolTipText("imagen");
			lbl_ProdImagen2.setBorder(null);
			lbl_ProdImagen2.setForeground(new Color(0, 0, 0));
			lbl_ProdImagen2.setBackground(new Color(0, 0, 0));
			lbl_ProdImagen2.setBounds(21, 230 + Posicion + 55, 200, 195);
			frame.getContentPane().add(lbl_ProdImagen2);
			
			JButton btnNewButton_AñadirCesta2 = new JButton("A\u00F1adir a la cesta");
			btnNewButton_AñadirCesta2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Añadir(Producto2, 2);
				}
			});
			btnNewButton_AñadirCesta2.setForeground(Color.WHITE);
			if (ProductoStock == 0)
				btnNewButton_AñadirCesta2.setEnabled(false);
			btnNewButton_AñadirCesta2.setFocusPainted(false);
			btnNewButton_AñadirCesta2.setBackground(new Color(0, 128, 0));
			btnNewButton_AñadirCesta2.setBounds(790, 230 + Posicion + 239, 135, 23);
			frame.getContentPane().add(btnNewButton_AñadirCesta2);
			
			if(usuario == "anonimo") 
				btnNewButton_AñadirCesta2.setEnabled(false);
			
			JLabel lblPrecio2 = new JLabel(ProductoPrecio + " €");
			lblPrecio2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
			lblPrecio2.setToolTipText("precio");
			lblPrecio2.setForeground(Color.BLACK);
			lblPrecio2.setBorder(null);
			lblPrecio2.setBackground(Color.BLACK);
			lblPrecio2.setBounds(241, 230 + Posicion + 124, 113, 39);
			frame.getContentPane().add(lblPrecio2);

			JLabel labelStock2 = new JLabel("Stock disponible: " + ProductoStock);
			labelStock2.setToolTipText("precio");
			if(ProductoStock <= 10)
				labelStock2.setForeground(Color.RED);
			else
				labelStock2.setForeground(Color.GRAY);
			if(ProductoStock == 0)
				labelStock2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
			else
				labelStock2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
			labelStock2.setBorder(null);
			labelStock2.setBackground(Color.WHITE);
			labelStock2.setBounds(241, 230 + Posicion + 150, 161, 32);
			frame.getContentPane().add(labelStock2);
			
			ListaProductos(2);
			
			JButton ButtonDetalleProducto3 = new JButton("");
			ButtonDetalleProducto3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
		            DescripcionProducto = Producto3;
		            CargarInterfaz cargar = new CargarInterfaz();
		            cargar.CargarDescripcionProducto(irListaProductos, DescripcionProducto, usuario, contrasenia);
                    frame.setVisible(false);				//Ocultamos InterfazRegistro
				}
			});
			ButtonDetalleProducto3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
			ButtonDetalleProducto3.setContentAreaFilled(false);
			ButtonDetalleProducto3.setIcon(null);
			ButtonDetalleProducto3.setBorder(null);
			ButtonDetalleProducto3.setBounds(20, 460 +54, 685, 193);
			frame.getContentPane().add(ButtonDetalleProducto3);
			frame.setBounds(100, 100, 961, 838);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			
			JLabel lbl_ProdNombre3 = new JLabel(ProductoNombre);
			lbl_ProdNombre3.setFont(new Font("Tahoma", Font.BOLD, 25));
			lbl_ProdNombre3.setToolTipText("nombre descripcion");
			lbl_ProdNombre3.setForeground(Color.BLACK);
			lbl_ProdNombre3.setBorder(null);
			lbl_ProdNombre3.setBackground(Color.BLACK);
			lbl_ProdNombre3.setBounds(231, 460 +Posicion + 75, 390, 50);
			Producto3 = lbl_ProdNombre3.getText();
			frame.getContentPane().add(lbl_ProdNombre3);
			
			// Redimensiona imagen de lblNewLabel_1
			Image Producto3img = new ImageIcon(ProductoImg).getImage();
			ImageIcon Producto3img1 = new ImageIcon(Producto3img.getScaledInstance(200, 200, Image.SCALE_SMOOTH));
			
			JLabel lbl_ProdImagen3 = new JLabel("");
			lbl_ProdImagen3.setIcon(Producto3img1);
			lbl_ProdImagen3.setToolTipText("imagen");
			lbl_ProdImagen3.setBorder(null);
			lbl_ProdImagen3.setForeground(new Color(0, 0, 0));
			lbl_ProdImagen3.setBackground(new Color(0, 0, 0));
			lbl_ProdImagen3.setBounds(21, 460 + Posicion + 55, 200, 195);
			frame.getContentPane().add(lbl_ProdImagen3);
			
			JButton btnNewButton_AñadirCesta3 = new JButton("A\u00F1adir a la cesta");
			btnNewButton_AñadirCesta3.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Añadir(Producto3, 3);
				}
			});
			btnNewButton_AñadirCesta3.setForeground(Color.WHITE);
			if (ProductoStock == 0)
				btnNewButton_AñadirCesta3.setEnabled(false);
			btnNewButton_AñadirCesta3.setForeground(Color.WHITE);
			btnNewButton_AñadirCesta3.setFocusPainted(false);
			btnNewButton_AñadirCesta3.setBackground(new Color(0, 128, 0));
			btnNewButton_AñadirCesta3.setBounds(790, 460 + Posicion + 239, 135, 23);
			frame.getContentPane().add(btnNewButton_AñadirCesta3);
			
			if(usuario == "anonimo") 
				btnNewButton_AñadirCesta3.setEnabled(false);
			
			JLabel lblPrecio3 = new JLabel(ProductoPrecio + " €");
			lblPrecio3.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
			lblPrecio3.setToolTipText("stock");
			lblPrecio3.setForeground(Color.BLACK);
			lblPrecio3.setBorder(null);
			lblPrecio3.setBackground(Color.BLACK);
			lblPrecio3.setBounds(241, 460 + Posicion + 124, 113, 39);
			frame.getContentPane().add(lblPrecio3);

			JLabel labelStock3 = new JLabel("Stock disponible: " + ProductoStock);
			labelStock3.setToolTipText("precio");
			if(ProductoStock <= 10)
				labelStock3.setForeground(Color.RED);
			else
				labelStock3.setForeground(Color.GRAY);
			if(ProductoStock == 0)
				labelStock3.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
			else
				labelStock3.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
			labelStock3.setBorder(null);
			labelStock3.setBackground(Color.WHITE);
			labelStock3.setBounds(241, 460 + Posicion + 150, 161, 32);
			frame.getContentPane().add(labelStock3);
			
			for (i=0; i < TotalProducto; i++) {
				if (i < Ventana) {
					
					JLabel labelEnvio = new JLabel("Recibelo en dos dias");
					labelEnvio.setToolTipText("");
					labelEnvio.setForeground(Color.GRAY);
					labelEnvio.setFont(new Font("Tahoma", Font.PLAIN, 12));
					labelEnvio.setBorder(null);
					labelEnvio.setBackground(Color.WHITE);
					labelEnvio.setBounds(241, Posicion + 174, 161, 22);
					frame.getContentPane().add(labelEnvio);

					JLabel lblNewLabel_3 = new JLabel(
							"\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014");
					lblNewLabel_3.setForeground(new Color(192, 192, 192));
					lblNewLabel_3.setBounds(10, Posicion + 261, 915, 14); // 261
					frame.getContentPane().add(lblNewLabel_3);

					JLabel label_10 = new JLabel(
							"\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014");
					label_10.setForeground(Color.LIGHT_GRAY);
					label_10.setBounds(10, 40, 915, 14);
					frame.getContentPane().add(label_10);

					

					Posicion += 230;
					System.out.print("Producto : "+ ProductoNombre);
				}else
					break;
			}
			System.out.print(i);
			i--;
			Ventana +=3;
		}

		JLabel lblNewLabel = new JLabel("Imagen");
		lblNewLabel.setIcon(new ImageIcon("img\\Fondo1.jpg"));
		lblNewLabel.setBounds(0, 0, 950, 800);
		frame.getContentPane().add(lblNewLabel);
	}

	public void ListaProductos(int vuelta) {
		if(buscar) {
			ProductoBuscar = textField.getText();
		}else
			ProductoBuscar = "%";
		try {
			// Conexión con administrador
			Class.forName("com.mysql.jdbc.Driver");
			Connection conectar = (Connection) DriverManager
					.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", usuario, contrasenia); //188.127.164.238

			// create the java statement
			Statement st = conectar.createStatement(); // Crear una conexión que luego se podra cerrar (administrador)

			/*********************************************************/
			String NumeroProductos = "SELECT COUNT(*) FROM APP_Tienda.PRODUCTOS WHERE Nombre LIKE '" + ProductoBuscar +"%'";

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(NumeroProductos);

			while (rs.next()) {
				TotalProducto = rs.getInt("COUNT(*)");
			}
			
			String query = "SELECT Nombre, Precio, Stock, Imagen FROM APP_Tienda.PRODUCTOS WHERE Nombre LIKE '" + ProductoBuscar + "%' LIMIT "+(TotalProducto-vuelta);

			// execute the query, and get a java resultset
			 rs = st.executeQuery(query);

			// iterate through the java resultset
			 while (rs.next()) {
					ProductoNombre = rs.getString("Nombre");					
					ProductoPrecio = rs.getString("Precio");					
					ProductoStock = rs.getInt("Stock");
					ProductoImg = rs.getString("Imagen");					
			}

			 buscar = true;

		//	DatosProducto(st, TotalProducto);
			
			st.close(); // Finalizar conexión (administrador)
		} catch (Exception e) {
			System.err.println("Ha habido un error");
			System.err.println(e.getMessage());
		}
		System.out.println(ProductoNombre);
	}
	
	public void Añadir (String Producto, int NumeroProducto) {
		if(NumeroProducto == 1) {
			Producto1Cesta = true;
			TotalCesta++;
		}
		
		if(NumeroProducto == 2) {
			Producto2Cesta = true;
			TotalCesta++;
		}
		
		if(NumeroProducto == 3) {
			Producto3Cesta = true;
			TotalCesta++;
		}
	}
	
	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public InterfazPrincipal getIrPrincipal() {
		return irPrincipal;
	}

	public void setIrPrincipal(InterfazPrincipal irPrincipal) {
		this.irPrincipal = irPrincipal;
	}

	public void InterfacesListaProductos(InterfazPrincipal irVerificar) {
		// TODO Auto-generated method stub
		
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String constrasenia) {
		this.contrasenia = constrasenia;
	}
	
}
