import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.border.BevelBorder;

import com.mysql.jdbc.Connection;

import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.awt.event.ActionEvent;

public class InterfazStockAdmin {

	private JFrame frmEditarStock;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazStockAdmin window = new InterfazStockAdmin();
					window.frmEditarStock.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the application.
	 */
	public InterfazStockAdmin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEditarStock = new JFrame();
		frmEditarStock.setTitle("Editar Stock");
		frmEditarStock.setBounds(100, 100, 544, 305);
		frmEditarStock.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEditarStock.getContentPane().setLayout(null);
		
		JLabel lblEditarStock = new JLabel("Editar Stock");
		lblEditarStock.setFont(new Font("Stencil", Font.PLAIN, 18));
		lblEditarStock.setBounds(31, 28, 237, 39);
		frmEditarStock.getContentPane().add(lblEditarStock);
		
		JLabel label = new JLabel("\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014\u2014");
		label.setForeground(Color.BLACK);
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		label.setBounds(26, 61, 537, 14);
		frmEditarStock.getContentPane().add(label);
		
		JLabel lblNewLabel = new JLabel("Identificador:");
		lblNewLabel.setBounds(26, 98, 86, 14);
		frmEditarStock.getContentPane().add(lblNewLabel);
		
		JLabel lblNombreProductos = new JLabel("Nombre del Producto:");
		lblNombreProductos.setBounds(26, 125, 133, 14);
		frmEditarStock.getContentPane().add(lblNombreProductos);
		
		textField = new JTextField();
		textField.setBounds(166, 95, 139, 20);
		frmEditarStock.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(166, 122, 139, 20);
		frmEditarStock.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblStock = new JLabel("Unidades:");
		lblStock.setBounds(26, 153, 108, 14);
		frmEditarStock.getContentPane().add(lblStock);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(166, 153, 139, 20);
		frmEditarStock.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setBounds(26, 236, 130, 20);
		frmEditarStock.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.setBounds(166, 235, 139, 23);
		frmEditarStock.getContentPane().add(btnConsultar);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBorder(new BevelBorder(BevelBorder.LOWERED, Color.BLACK, null, null, null));
		lblNewLabel_1.setBounds(333, 86, 175, 170);
		frmEditarStock.getContentPane().add(lblNewLabel_1);
		
		JButton btnI = new JButton("Modificar");

		btnI.setBounds(26, 199, 130, 23);
		frmEditarStock.getContentPane().add(btnI);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setBounds(166, 199, 139, 23);
		frmEditarStock.getContentPane().add(btnEliminar);
		
		// events
		btnI.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Connection con = null;
				try {
					Class.forName("com.mysql.jdbc.Driver");
		            con = (Connection) DriverManager.getConnection("jdbc:mysql://188.127.164.238:3306/APP_Tienda", "Admin", "admin@dosa_2019");
				}catch (Exception e) {
					e.printStackTrace();
				}
				String insertar="INSERT INTO PRODUCTOS(ID, Nombre, Stock) VALUES (?, ?, ?)";
			}
		});
	}
}
