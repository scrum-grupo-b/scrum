import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.*;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import java.awt.Toolkit;

public class InterfazVerificar {

    private JFrame frame;
    InterfazPrincipal irVerificar;
    private JTextField textField;
    boolean validado = false;

    //Creamos variables para guardar los datos que nos pasan
    InterfazRegistro ir;
    String codigover;
    String correo_electronico;
    String password;
    String nombre;
    String apellidos;
    int ID;

    /**
     * Launch the application.
     */


    /**
     * Create the application.
     */
    public InterfazVerificar(InterfazRegistro ir, String codigover, String correo_electronico, String password, String nombre, String apellidos) {
        initialize();
        this.ir = ir;
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.getContentPane().setBackground(Color.WHITE);
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\Logotipo.png"));
        frame.setTitle("Menu Verificar");
        frame.getContentPane().setForeground(Color.WHITE);
        frame.setBounds(100, 100, 866, 658);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);
        frame.setLocationRelativeTo(null);

        JButton btnVerificar = new JButton("Verificar");
        btnVerificar.setFocusPainted(false);
        btnVerificar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                validado = Verificar();
                if(validado) {
                	InterfacesListaProductos cargar = new InterfacesListaProductos(irVerificar, "anonimo", "anonimo");
    				cargar.InterfacesListaProductos(irVerificar);
    				cargar.getFrame().setVisible(true); 
    				frame.setVisible(false);
                }
                else
                	JOptionPane.showMessageDialog(null, "No se ha podido verificar la cuenta");
                	
            }
        });
        btnVerificar.setBounds(247, 269, 274, 23);
        btnVerificar.setFont(new Font("Microsoft YaHei", Font.BOLD, 12));
        frame.getContentPane().add(btnVerificar);
        
        JLabel lblVerificaTuCuenta = new JLabel("Verifica tu cuenta");
        lblVerificaTuCuenta.setForeground(Color.BLACK);
        lblVerificaTuCuenta.setFont(new Font("Microsoft YaHei", Font.BOLD, 25));
        lblVerificaTuCuenta.setBackground(SystemColor.menu);
        lblVerificaTuCuenta.setBounds(247, 90, 254, 34);
        frame.getContentPane().add(lblVerificaTuCuenta);

        JLabel lblCodigo = new JLabel("Codigo");
        lblCodigo.setForeground(Color.BLACK);
        lblCodigo.setBounds(247, 206, 274, 14);
        frame.getContentPane().add(lblCodigo);

        JLabel lblNewLabel_1 = new JLabel("Hemos enviado un correo a la direccion indicada," );
        lblNewLabel_1.setBounds(247, 151, 289, 23);
        frame.getContentPane().add(lblNewLabel_1);
        
        JLabel lblNewLabel = new JLabel("copie el codigo e insertelo para poder registrarte");
        lblNewLabel.setBounds(247, 168, 289, 14);
        frame.getContentPane().add(lblNewLabel);

        textField = new JTextField();
        textField.setBounds(247, 224, 274, 20);
        frame.getContentPane().add(textField);
        textField.setColumns(10);
        
        JTextPane txtpnHemosEnviadoUn = new JTextPane();
        txtpnHemosEnviadoUn.setToolTipText("");
        txtpnHemosEnviadoUn.setForeground(Color.BLACK);
        txtpnHemosEnviadoUn.setEditable(false);
        txtpnHemosEnviadoUn.setBorder(new LineBorder(Color.BLACK));
        txtpnHemosEnviadoUn.setBackground(Color.WHITE);
        txtpnHemosEnviadoUn.setBounds(221, 56, 332, 460);
        frame.getContentPane().add(txtpnHemosEnviadoUn);
    }

    
    //Verificamos codigo (token)
    public boolean Verificar() {
        String codigoescrito = textField.getText();
        if(this.codigover.equals(codigoescrito)) {
            try {
                //Conexi�n con administrador
                Class.forName("com.mysql.jdbc.Driver");
                Connection conectar = (Connection) DriverManager.getConnection("jdbc:mysql://188.127.164.238:3306", "admin", "admin@dosa_2019"); //188.127.164.238

                // create the java statement
                Statement st = conectar.createStatement();		//Crear una conexi�n que luego se podra cerrar (administrador)

                /*********************************************************/

                //SQL query
                /*			String query = "SELECT * FROM APP_Tienda.PRODUCTOS";

                // execute the query, and get a java resultset
                ResultSet rs = st.executeQuery(query);

                // iterate through the java resultset
                while (rs.next())
                {
                int ID = rs.getInt("ID");
                String Nombre = rs.getString("Nombre");
                double Precio = rs.getDouble("Precio");
                String Detalles = rs.getString("Detalles");
                String Categoria = rs.getString("Categoria");

                // print the results
                System.out.format("%s, %s, %s, %s, %s\n", ID, Nombre, Precio, Detalles, Categoria);
                }
                */
                /***********************************************************/
                //PreparedStatement para create user
                PreparedStatement createuser = conectar.prepareStatement("CREATE USER IF NOT EXISTS ? IDENTIFIED BY ?");

                //Creamos el apodo (nombre de usuario en base de datos) que sera su nombre de correo hasta el @
                CrearApodoUsuario apodo = new CrearApodoUsuario();
                String apodousuario = apodo.Apodo(correo_electronico);

                String NombreUser = apodousuario;
                String PasswordUser = password;

                createuser.setString(1,NombreUser);
                createuser.setString(2,PasswordUser);

                //Ejecutar create user y privileges
                createuser.executeUpdate();

                /***********************************************************/
                //PreparedStatement para insert
                PreparedStatement insertuser = conectar.prepareStatement("INSERT INTO APP_Tienda.CLIENTES(Nombre, Apellidos, CorreoElectronico, Apodo) VALUES (?,?,?,?)");

                //Insertar datos en la sentencia insert
                String NombreInsert = nombre;
                String ApellidoInsert = apellidos;
                String Correo_Electronico = correo_electronico;

                insertuser.setString(1,NombreInsert);
                insertuser.setString(2,ApellidoInsert);
                insertuser.setString(3,Correo_Electronico);
                insertuser.setString(4,apodousuario);

                //Ejecutar insert
                insertuser.executeUpdate();

                /***********************************************************/
                //PreparedStatement para create user view

                String NombreUserView = apodousuario;
                String CorreoElectronico = correo_electronico;
                String CreateUserView = "CREATE VIEW APP_Tienda." + NombreUserView + "_view AS SELECT * FROM APP_Tienda.CLIENTES WHERE CorreoElectronico = '" + CorreoElectronico + "'";

                PreparedStatement createview = conectar.prepareStatement(CreateUserView);

                //	    String NombreUserView = "prueba";
                //	    String CorreoElectronico = "arturo.sierra.98@gmail.com";

                //	    createview.setString(1,NombreUserView);
                //	    createview.setString(2,CorreoElectronico);

                //Ejecutar create user y privileges
                createview.executeUpdate();

                /***********************************************************/   //tengo que hacer select para saber la id del cliente creado :C
                //Consulta saber ID cliente
                String query = "SELECT * FROM APP_Tienda.CLIENTES";

                ResultSet rs = st.executeQuery(query);

                // iterate through the java resultset
                while (rs.next())
                {
                    ID = rs.getInt("ID");
                }
                //PreparedStatement para create user view pedidos
                String NombreUserPedidos = apodousuario;
                int ID_Cliente = ID;
                String CreateUserPedidos = "CREATE VIEW APP_Tienda." + NombreUserPedidos + "_viewPedidos AS SELECT * FROM APP_Tienda.PEDIDOS WHERE ID_Cliente = '" + ID_Cliente + "'";

                PreparedStatement createviewpedido = conectar.prepareStatement(CreateUserPedidos);

                //Ejecutar create user y privileges
                createviewpedido.executeUpdate();

                /***********************************************************/
                //PreparedStatement para create user view detalle pedidos

                String NombreUserPedidoDetalle = apodousuario;
                int ID_ClienteDetallePedidos = ID;
                String CreateUserPedidosDetalle = "CREATE VIEW APP_Tienda." + NombreUserPedidoDetalle + "_viewPedidoDetalle AS SELECT * FROM APP_Tienda.DETALLEPEDIDOS WHERE ID_Cliente = '" + ID_ClienteDetallePedidos + "'";

                PreparedStatement createviewdetallepedido = conectar.prepareStatement(CreateUserPedidosDetalle);

                //Ejecutar create user y privileges
                createviewdetallepedido.executeUpdate();

                /***********************************************************/

                //PreparedStatement para grant view

                String Nombreview = apodousuario + "_view";
                String NombreUserPrivileges = apodousuario;
                String Grant = "GRANT SELECT, UPDATE ON APP_Tienda." + Nombreview + " TO " + NombreUserPrivileges;

                PreparedStatement privileges = conectar.prepareStatement(Grant);

                //Ejecutar privileges
                privileges.executeUpdate();

                /***********************************************************/

                //PreparedStatement para grant pedidos

                String Nombreviewpedidos = apodousuario + "_viewPedidos";
                String NombreUserPrivilegespedidos = apodousuario;
                String Grantpedidos = "GRANT SELECT ON APP_Tienda." + Nombreviewpedidos + " TO " + NombreUserPrivilegespedidos;

                PreparedStatement privilegespedidos = conectar.prepareStatement(Grantpedidos);

                //Ejecutar privileges
                privilegespedidos.executeUpdate();

                /***********************************************************/

                //PreparedStatement para grant detalle pedidos

                String Nombreviewdetallepedidos = apodousuario + "_viewPedidoDetalle";
                String NombreUserPrivilegesdetallepedidos = apodousuario;
                String Grantdetallepedidos = "GRANT SELECT ON APP_Tienda." + Nombreviewdetallepedidos + " TO " + NombreUserPrivilegesdetallepedidos;

                PreparedStatement privilegesdetallepedidos = conectar.prepareStatement(Grantdetallepedidos);

                //Ejecutar privileges
                privilegesdetallepedidos.executeUpdate();
                
                /***********************************************************/
                
                //PreparedStatement para grant descuentos

                String NombreUserdescuentos = apodousuario;
                String Grantdescuentos = "GRANT SELECT ON APP_Tienda.DESCUENTOS TO " + NombreUserdescuentos;

                PreparedStatement privilegesdescuentos = conectar.prepareStatement(Grantdescuentos);

                //Ejecutar privileges
                privilegesdescuentos.executeUpdate();

                /***********************************************************/

                //PreparedStatement para grant productos

                String NombreUserpedidos = apodousuario;
                String GrantProductos = "GRANT SELECT ON APP_Tienda.PRODUCTOS TO " + NombreUserpedidos;

                PreparedStatement privilegesProductos = conectar.prepareStatement(GrantProductos);

                //Ejecutar privileges
                privilegesProductos.executeUpdate();
                
                validado = true;

                st.close();			//Finalizar conexi�n (administrador)
            }
            catch (Exception e)
            {
                System.err.println("Ha habido un error");
                System.err.println(e.getMessage());
            }
        }
        
		return validado;

    }
    
  //Creamos get y set para obtener el dato que nos pasan desde InterfazRegistro y lo guardamos en las variables
    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public String getCodigover() {
        return codigover;
    }

    public void setCodigover(String codigover) {
        this.codigover = codigover;
    }

    public String getCorreo_electronico() {
        return correo_electronico;
    }

    public void setCorreo_electronico(String correo_electronico) {
        this.correo_electronico = correo_electronico;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

}
