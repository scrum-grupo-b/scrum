
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validar {

    public boolean ValidarCorreo(String email) {

        // Patr�n para validar el email
        Pattern pattern = Pattern
            .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

        // El email a validar
        //      String email = "usuariodestinatario@gmail.com";

        Matcher mather = pattern.matcher(email);

        if (mather.find() == true)
            return true;
        //        System.out.println("El email ingresado es v�lido.");
        else
            return false;
        //         System.out.println("El email ingresado es inv�lido.");
    }

    public boolean ValidarContrasenia(String password) {

        //1 mayuscula, 1 minuscula, 1 numero minimo
        int length = password.length();
        char clave;
        byte  contNumero = 0,
              contLetraMay = 0,
              contLetraMin = 0,
              contEspe = 0;
        boolean validaContrasenia = false;

        for (byte i = 0; i < password.length(); i++) {
            clave = password.charAt(i);
            String passValue = String.valueOf(clave);
            if (passValue.matches("[A-Z]")) {
                contLetraMay++;
            } else if (passValue.matches("[a-z]")) {
                contLetraMin++;
            } else if (passValue.matches("[0-9]")) {
                contNumero++;
            } else if (passValue.matches("[@._-]")) {
                contEspe++;
            }
        }

        if(contLetraMay > 0 && contLetraMin > 0 && contNumero > 0 && contEspe > 0
                && length > 7)
            validaContrasenia = true;
        else
            validaContrasenia = false;

        return validaContrasenia;

        /*	        System.out.println("Cantidad de letras mayusculas:"+contLetraMay);
                        System.out.println("Cantidad de letras minusculas:"+contLetraMin);
                        System.out.println("Cantidad de numeros:"+contNumero);
                        System.out.println("Cantidad de especiales:"+contEspe);
                        System.out.println("Cantidad de total: " + length);
                        System.out.println(validaContrasenia);
                        */


    }

}
