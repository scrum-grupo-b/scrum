import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class InterfazAdmin {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfazAdmin window = new InterfazAdmin();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfazAdmin() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 859, 704);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("BuyMe ADMIN PANEL");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Franklin Gothic Book", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel.setBounds(10, 11, 179, 24);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblPanelDeControl = new JLabel("Panel de Control:");
		lblPanelDeControl.setForeground(Color.BLACK);
		lblPanelDeControl.setFont(new Font("Franklin Gothic Book", Font.PLAIN, 15));
		lblPanelDeControl.setBounds(10, 46, 179, 24);
		frame.getContentPane().add(lblPanelDeControl);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"...", "Usuarios", "Productos", "Compras", "Ventas", "Pedidos"}));
		comboBox.setToolTipText("Ferreteria\r\nJugueteria\r\nDeportes");
		comboBox.setFont(new Font("Source Sans Pro Black", Font.BOLD, 11));
		comboBox.setBounds(10, 81, 111, 20);
		frame.getContentPane().add(comboBox);
		
		JLabel lblNewLabel_1 = new JLabel("New label");
		lblNewLabel_1.setIcon(new ImageIcon("img\\AdminFondo.jpg"));
		lblNewLabel_1.setBounds(0, 0, 843, 665);
		frame.getContentPane().add(lblNewLabel_1);
	}

}
