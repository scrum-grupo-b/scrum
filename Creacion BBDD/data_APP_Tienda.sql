USE APP_Tienda;

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Martillo encofrador 27mm', 'Ferreteria', 15.95, 'Martillo encofrador pulido 27mm cfibr mg27f (27mm cabo fibra de vidrio 453g)', 10, '2018-02-15', 'MaxiFerreteria', 'España', 'img\\Productos\\Martillo_encofrador.jpg'
);
INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Loctite mini trio 3x1g', 'Ferreteria', 11.65, 'Loctite mini trio 3x1g practico envase de una dosis adhesivo universal instantaneo maxima fuerza union en pocos segundos nueva formula resitente al agua y al lavavajillas para pegar en pocos segundos todos los tipos de materiales: porcelana, madera, plas', 29, '2019-01-09', 'Leroy Merlin', 'Italia', 'img\\Productos\\Loctite_mini_trio.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Manguera jardin basic 15m', 'Ferreteria', 17.99, 'Manguera jardin basic line ø interior 15mm ø exterior 20mm (5/8") - rollo 15mts edm', 3, '2017-10-27', 'Bricomart', 'España', 'img\\Productos\\Mangera_jardin_basic_15m.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Taladro percutor 500W', 'Ferreteria', 36.80, 'Taladro percutor /atornillador - electronico - potencia: 500w - portabrocas automatico ø13mm - 0-2600rpm - tacto de goma - mango ergonomico - mango adicional con varilla de profundidad - 2mts de cable - control de potencia - dimensiones: 20x27cm - 220/2', 67, '2018-06-06', 'MaxiFerreteria', 'EEUU', 'img\\Productos\\Taladro_percutor_500W.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Fijacuadros Fischer 8 Unds', 'Ferreteria', 2.54, 'Esta fijación puede ser aplicada por cualquier persona y de manera manual gracias a la gran facilidad que tiene al fijar el producto. El fijacuadros Fischer puede colgar cualquier objeto de cualquier tipo con una carga máxima de 8 kilogramos', 11, '2019-07-11', 'LetsBrico', 'España', 'img\\Productos\\Fijador_fischer_8unds.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Algodón 90 Minnie Rosa', 'Confeccion', 1.60, 'Tela de algodón 100% Ancho de 110cm Nuestra mascota Metrín, mide 10cm de largo. Venta mínima de 3 unidades (30cm) 1 unidad= 10cm', 3, '2016-03-12', 'Megustacoser', 'España', 'img\\Productos\\Algodon_90_minnie_mouse_rosa.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Doble Gasa Algodón Flor', 'Confeccion', 1.69, 'Tela de algodón 100% Ancho de 130cm 130gr/m2 Venta mínima de 3 unidades (30cm) 1 unidad= 10cm Lavar a 30º, no usar lejías ni secadora, plancha temperatura media', 56, '2018-05-05', 'Telas Rosa', 'India', 'img\\Productos\\Algodon_doble_gasa.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Alfiler cabezera vidrio', 'Confeccion', 3.95, 'Alfileres con un largo de 30mm y grosor de 0.60mm. Con cabeza de vidrio. Presentación en cajita de 10 gr.', 13, '2018-04-09', 'Megustacoser', 'India', 'img\\Productos\\Alfiler_cabezera_vidrio.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Bastidor cuadrado', 'Confeccion', 14.25, 'Medidas Talla M: 28x28cm / 11"x11" Talla L: 43x43cm/ 17"x17" Bastidor ligero, ideal para todo tipo de manualidades, bordados y acolchados. Fácil de montar y desmontar. Pinzas largas para sostener la tela de forma segura. Proporciona buena tensión. Esquinas redondeadas y bordes suaves para proteger la tela.', 21, '2018-03-08', 'Coser y cantar', 'Indonesia', 'img\\Productos\\Bastidor_cuadrado.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Regla Patchwork pulgadas', 'Confeccion', 16.90, 'Regla en pulgadas 6.5x24 pulgadas  Fabricada con material acrílico transparente y duradero. Borde liso para cortes de precisión. Sencillo y fácil de leer las líneas de cuadrícula y medidas', 16, '2019-03-09', 'Coser y cantar', 'España', 'img\\Productos\\Regla_patchwork_pulgadas.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Mimitos niño sencillo', 'Jugueteria', 21.99, 'El bebé más famoso de Juguettos se llama Mimittos. Lleva muchos años proporcionando momentos muy felices a niños y niñas que juegan a se papás. Contenido: 1 bebé mimitos, 1 biberón y 1 chupete. Medidas: 43 cm', 30, '2017-02-02', 'Juguettos', 'China', 'img\\Productos\\Mimitos_ninio_sencillo.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'T-Toca linea 4 acuatico', 'Jugueteria', 9.99,  'Si quieres ganar consigue 4 fichas en línea de tu color antes que tu oponente. Puedes hacer la líne: en vertical, diagonal y horizontal.
Contiene un compartimento para guardar las fichas, además, ¡es plegable!. Contenido: 1 tablero plegable y 42 fichas en 2 colores', 9, '2019-02-02', 'Toy-Saras', 'China', 'img\\Productos\\T-toca_linea_4_acuatico.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Mallas pilates mujer', 'Deportes', 24.99, 'Leggings moldeadores y transpirables para ofrecer mayor sujeción gracias a la cintura alta. Acompañará tus sesiones de gimnasia', 39, '2019-02-20', 'Decathlon', 'España', 'img\\Productos\\Mallas_pilates_mujer.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Xiaomi Mi Air 13"', 'Informatica', 799, 'El Xiaomi Mi Notebook Aires ligero, es compacto, y podrás llevarlo fácilmente a cualquier lugar sin darte cuenta de que lo llevas encima. Fabricado en metal de gran calidad y con unos acabados de primera, el portátil de Xiaomi hace presume de una extrema delgadez de tan solo 14.8 milímetros y 1,3 Kg de peso. En tan reducidas dimensiones disfrutamos de una pantalla de 13,3 pulgadas con resolución Full HD y cámara HD sin apenas marcos.', 57, '2019-02-08', 'PCComponentes', 'España', 'img\\Productos\\Xiaomi_mi_notebook_air_13.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Samsung UE40NU7125 40"', 'Informatica', 349, 'Resolución 4K UHD real.Disfruta de imágenes nítidas y brillantes con el televisor 4K UHD, con una resolución cuatro veces superior a la del Full HD. Prepárate para percibir hasta el más mínimo detalle en todas tus escenas.', 22, '2017-02-20', 'InfoTec', 'EEUU', 'img\\Productos\\Samsung_UE40NU.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'Corsair H100i RGB', 'Informatica', 134.99, 'CORSAIR Hydro Series H100i RGB PLATINUM es un enfriador de CPU líquido todo en uno con un radiador de 240 mm y una iluminación RGB diseñada para una refrigeración extrema de la CPU. Equipado con 24 LED RGB individuales, el H100i RGB PLATINUM ofrece una gran variedad de opciones de iluminación RGB a través del software CORSAIR iCUE.', 2, '2018-05-06', 'PCComponentes', 'España', 'img\\Productos\\Corsair_H100i_RGB.jpg'
);

INSERT INTO PRODUCTOS (Nombre, Categoria, Precio, Detalles, Stock, Fecha, Proveedor, PaisOrigen, Imagen) VALUES
(
        'FIFA 19', 'Videojuegos', 59.96, 'Gracias al motor Frostbite, EA SPORTS FIFA 19 ofrece una experiencia del calibre de un campeón tanto dentro como fuera del campo. Liderado por la prestigiosa UEFA Champions League, FIFA 19 ofrece funciones de juego mejoradas que te permiten tener el control del terreno de juego en todo momento y ofrece otras formas de jugar, incluyendo un final dramático en la historia de Alex Hunter en El Camino: Champions, un otro modo de juego en el conocido FIFA Ultimate Team, y más', 14, '2019-01-08', 'GAME', 'EEUU', 'img\\Productos\\Fifa_2019.jpg'
);

INSERT INTO PEDIDOS VALUES (1,1,1,'2019-02-02', 'Llamar al timbre de la izquierda', 13.8, '2019-02-05');

INSERT INTO DETALLEPEDIDOS VALUES(1,1,1,2,5.78);
